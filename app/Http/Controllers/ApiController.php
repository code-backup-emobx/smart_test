<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\AllUsers;
use App\Models\AdminCategory;
use App\Models\SubjectDetail;
use App\Models\AddQuestion;
use DB;

class ApiController extends Controller {

	public function login(Request $request)
	{

		$email = $request->input('email');
		$password = $request->input('password');
        $check_user = User::where(["email" => $email])->where("user_type","super_admin")->first();
        $check_admin = User::where(["email" => $email])->where("user_type","admin")->first();
		if (!empty($check_user) && Hash::check($password, $check_user->password)) {
		  	
		  	$message = "Welcome Back ".$check_user->name;
            $sadmin_detail_result['login_admin_detail'] = array(
            	"id" => $check_user->id,
            	"name" => $check_user->name,
            	"email" => $check_user->email
            );
            $sadmin_detail_result['error_code'] = '0';
            $sadmin_detail_result['message'] = $message;
            return response()->json($sadmin_detail_result);
		   
		}
		else if(!empty($check_admin) && Hash::check($password , $check_admin->password)){

			$message = "Welcome Back ".$check_admin->name;
            $admin_detail_result['login_admin_detail'] = array(
            	"id" => $check_admin->id,
            	"name" => $check_admin->name,
            	"email" => $check_admin->email
            );
            $admin_detail_result['error_code'] = '0';
            $admin_detail_result['message'] = $message;
            return response()->json($admin_detail_result);

		}
		else{
			
			$message = "Please check your email id and password .....";
			$fail_message['error_code'] = "404";
			$fail_message['message'] =  $message;
			return response()->json($fail_message);
		}
		
	}

	public function add_multiple_admin(Request $request){

		$admin_name = $request->input('admin_name');
		$admin_phone_number = $request->input('admin_phone_number');
		$admin_email = $request->input('admin_email');
		$admin_password = $request->input('admin_password');

		$check_email = AllUsers::where('email',$admin_email)->count();
		if ($check_email > 0) {
            return response()->json(["err_code" => "500", "message" => "This Email id is already exists."]);
        }
        if(!$admin_name)
        {
            return response()->json(["error_code" => "500", "message" => "name field should be required"]);
        }
        if( !preg_match("/^([a-zA-Z' ]+)$/", $admin_name))
        {
            return response()->json(["error_code" => "500", "message" => "name shoud be alphabet"]);
        }
        if(!$admin_phone_number)
        {
            return response()->json(["error_code" => "500", "message" => "phone_number field should be required"]);
        }
         if( !preg_match("^((\\+91-?)|0)?[0-9]{9}$^", $admin_phone_number))
        {
            return response()->json(["error_code" => "500", "message" => "phone number shoud be in digits and 10 number only"]);
        }

		$check_user = AllUsers::where('user_type','super_admin')->get();
		if($check_user){

			$add_admin = new AllUsers();
			$add_admin->name = $admin_name;
			$add_admin->email = $admin_email;
			$add_admin->phone_no = $admin_phone_number;
			$add_admin->password = Hash::make($admin_password);
			$add_admin->admin_password = $admin_password;
			$add_admin->save();
			
			$add_admin_result = array();
			$add_admin_result["added_admin_detail"] = $add_admin;
			$add_admin_result["error_code"] = "0";
			$add_admin_result["message"] = "Admin detail added successfully";
			return response()->json($add_admin_result);

		}
		else{

			return $message = "Something Went wrong";
		}

	}

	public function multiple_admins_list(){

		// dd(12);
		$added_admins_list = AllUsers::where('user_type','=','admin')->get();

		$admins_list_result	= array();
		$admins_list_result["list_of_multiple_admins"] = $added_admins_list;
		// print_r($admins_list_result);die();
		$admins_list_result['error_code'] = "0";
		$admins_list_result["message"] = "list of multiple admins added by super admin";
		return response()->json($admins_list_result);
	
	}

	/** admin add category questions**/

	public function add_questions_category(Request $request){

        $category_name = $request->input('category_name');
        $login_admin_name = $request->input('login_admin_name');
        $login_admin_id = $request->input('login_admin_id');

        $question_category_save = new AdminCategory();
        $question_category_save->category_name = $category_name;
        $question_category_save->login_admin_name = $login_admin_name;
        $question_category_save->login_admin_id = $login_admin_id;
        $question_category_save->save();

        $saved_category_result = array();
        $saved_category_result['add_questions_category'] = $question_category_save;
        $saved_category_result['error_code'] = "0";
        $saved_category_result['message'] = "Question category added Successfully";
        return response()->json($saved_category_result);

	}

	/** admin add subjects of questions**/
	public function add_question_subject(Request $request){

		$subject_name = $request->input('subject_name');
		$category_id = $request->input('category_id');
		$duration = $request->input('duration');
		$login_admin_id = $request->input('login_admin_id');
		$login_admin_name = $request->input('login_admin_name');

		$add_question_subject = new SubjectDetail();
		$add_question_subject->subject_name = $subject_name;
		$add_question_subject->category_id = $category_id;
		$add_question_subject->duration = $duration;
		$add_question_subject->login_admin_name = $login_admin_name;
		$add_question_subject->login_admin_id = $login_admin_id;
		$add_question_subject->save();

		$add_subject_result = array();
		$add_subject_result['add_subject_detail'] = $add_question_subject;
		$add_subject_result['error_code'] = "0";
		$add_subject_result['message'] = 'Subject Added Successfully';
		return response()->json($add_subject_result);		
	}

	/** start multiple admin add questions **/ 

	//** add multiple choice qquestions **//
	public function add_mcqs_question(Request $request){

		$question_text = $request->input('question_text');
		$option_one = $request->input('option_one');
		$option_two = $request->input('option_two');
		$option_three = $request->input('option_three');
		$option_four = $request->input('option_four');
		$correct_answer = $request->input('correct_answer');
		$subject_name = $request->input('subject_name');
		$category_id = $request->input('category_id');
		$login_admin_id = $request->input('login_admin_id');
		$login_admin_name = $request->input('login_admin_name');

		$add_mcqs_question = new AddQuestion();
		$add_mcqs_question->question_text = $question_text;
		$add_mcqs_question->option_one = $option_one;
		$add_mcqs_question->option_two = $option_two;
		$add_mcqs_question->option_three = $option_three;
		$add_mcqs_question->option_four = $option_four;
		$add_mcqs_question->correct_answer = $correct_answer;
		$add_mcqs_question->subject_name = $subject_name;
		$add_mcqs_question->category_id = $category_id;
		$add_mcqs_question->login_admin_name = $login_admin_name;
		$add_mcqs_question->login_admin_id =$login_admin_id;
		$add_mcqs_question->question_type = "mcqs";
		$add_mcqs_question->save();

		$add_mcqs_question_result = array();
		$add_mcqs_question_result['add_multiple_choice_question'] = $add_mcqs_question;
		$add_mcqs_question_result['error_code'] = '0';
		$add_mcqs_question_result['message'] = 'Multiple Choice Question Added Successfully';
		return response()->json($add_mcqs_question_result);
	} 

	//* add multiple response questions*/ 

	public function add_multiple_response_questions(Request $request){

		$question_text = $request->input('question_text');
		$option_one = $request->input('option_one');
		$option_two = $request->input('option_two');
		$option_three = $request->input('option_three');
		$option_four = $request->input('option_four');
		$correct_answer = $request->input('correct_answer');
		$subject_name = $request->input('subject_name');
		$category_id = $request->input('category_id');
		$login_admin_id = $request->input('login_admin_id');
		$login_admin_name = $request->input('login_admin_name');

		$add_mrqs_question = new AddQuestion();
		$add_mrqs_question->question_text = $question_text;
		$add_mrqs_question->option_one = $option_one;
		$add_mrqs_question->option_two = $option_two;
		$add_mrqs_question->option_three = $option_three;
		$add_mrqs_question->option_four = $option_four;
		$add_mrqs_question->correct_answer = $correct_answer;
		$add_mrqs_question->subject_name = $subject_name;
		$add_mrqs_question->category_id = $category_id;
		$add_mrqs_question->login_admin_name = $login_admin_name;
		$add_mrqs_question->login_admin_id =$login_admin_id;
		$add_mrqs_question->question_type = "mrqs";
		$add_mrqs_question->save();

		$add_mrqs_question_result = array();
		$add_mrqs_question_result['add_multiple_respose_question'] = $add_mrqs_question;
		$add_mrqs_question_result['error_code'] = "0";
		$add_mrqs_question_result['message'] = "Multiple Response Question Added Successfully";
		return response()->json($add_mrqs_question_result);
	}

	/* add true false type question*/

	public function add_true_false_questions(Request $request){

		$question_text = $request->input('question_text');
		$option_one = $request->input('option_one');
		$option_two = $request->input('option_two');
		$correct_answer = $request->input('correct_answer');
		$subject_name = $request->input('subject_name');
		$category_id = $request->input('category_id');
		$login_admin_id = $request->input('login_admin_id');
		$login_admin_name = $request->input('login_admin_name');

		$add_true_false_question = new AddQuestion();
		$add_true_false_question->question_text = $question_text;
		$add_true_false_question->option_one = $option_one;
		$add_true_false_question->option_two = $option_two;
		$add_true_false_question->correct_answer = $correct_answer;
		$add_true_false_question->subject_name = $subject_name;
		$add_true_false_question->category_id = $category_id;
		$add_true_false_question->login_admin_name = $login_admin_name;
		$add_true_false_question->login_admin_id =$login_admin_id;
		$add_true_false_question->question_type = "true_false";
		$add_true_false_question->save();

		$add_true_false_question_result = array();
		$add_true_false_question_result['add_multiple_choice_question'] = $add_true_false_question;
		$add_true_false_question_result['error_code'] = "0";
		$add_true_false_question_result['message'] = "True False Question Added Successfully";
		return response()->json($add_true_false_question_result);	
	}

	/*add short type questions*/

	public function add_short_question(Request $request){

		$question_text = $request->input('question_text');
		$correct_answer = $request->input('correct_answer');
		$subject_name = $request->input('subject_name');
		$category_id = $request->input('category_id');
		$login_admin_id = $request->input('login_admin_id');
		$login_admin_name = $request->input('login_admin_name');

		$add_short_question = new AddQuestion();
		$add_short_question->question_text = $question_text;
		$add_short_question->correct_answer = $correct_answer;
		$add_short_question->subject_name = $subject_name;
		$add_short_question->category_id = $category_id;
		$add_short_question->login_admin_name = $login_admin_name;
		$add_short_question->login_admin_id =$login_admin_id;
		$add_short_question->question_type = "short_answer";
		$add_short_question->save();

		$add_short_question_result = array();
		$add_short_question_result['add_multiple_choice_question'] = $add_short_question;
		$add_short_question_result['error_code'] = "0";
		$add_short_question_result['message'] = "Short Question Added Successfully";
		return response()->json($add_short_question_result);

	}

	/* add numeric type questions*/ 
	public function add_numeric_question(Request $request){

		$question_text = $request->input('question_text');
		$correct_answer = $request->input('correct_answer');
		$numeric_picture = $request->input('numeric_picture');
		$subject_name = $request->input('subject_name');
		$category_id = $request->input('category_id');
		$login_admin_id = $request->input('login_admin_id');
		$login_admin_name = $request->input('login_admin_name');

		if ($request->hasFile('numeric_picture') != "") {
            $image = $request->file('numeric_picture');
            // $filename = time() . '.' . $image->getClientOriginalExtension();
            $filename =$image->getClientOriginalName();
            $destinationPath = public_path('/numeric_pictures');
            $image->move($destinationPath, $filename);
            $numeric_picture = '/numeric_pictures/' . $filename;
        } else {
            $numeric_picture = "";
        }

		$add_numeric_question = new AddQuestion();
		$add_numeric_question->question_text = $question_text;
		$add_numeric_question->correct_answer = $correct_answer;
		$add_numeric_question->numeric_picture = $numeric_picture;
		$add_numeric_question->subject_name = $subject_name;
		$add_numeric_question->category_id = $category_id;
		$add_numeric_question->login_admin_name = $login_admin_name;
		$add_numeric_question->login_admin_id =$login_admin_id;
		$add_numeric_question->question_type = "numeric_answer";
		$add_numeric_question->save();

		$media_url = url('/');
		$add_numeric_question_result = array();
		$add_numeric_question_result['add_multiple_choice_question'] = $add_numeric_question;
		$add_numeric_question_result['error_code'] = "0";
		$add_numeric_question_result['message'] = "Numeric Question Added Successfully";
		$add_numeric_question_result['numeric_media_base_url'] = $media_url;
		return response()->json($add_numeric_question_result);
	}

	/* add essay type questions*/ 
	public function add_essay_question(Request $request){

		$question_text = $request->input('question_text');
		$correct_answer = $request->input('correct_answer');
		$subject_name = $request->input('subject_name');
		$category_id = $request->input('category_id');
		$login_admin_id = $request->input('login_admin_id');
		$login_admin_name = $request->input('login_admin_name');

		$add_essay_question = new AddQuestion();
		$add_essay_question->question_text = $question_text;
		$add_essay_question->correct_answer = $correct_answer;
		$add_essay_question->subject_name = $subject_name;
		$add_essay_question->category_id = $category_id;
		$add_essay_question->login_admin_name = $login_admin_name;
		$add_essay_question->login_admin_id =$login_admin_id;
		$add_essay_question->question_type = "essay_question";
		$add_essay_question->save();

		$add_essay_question_result = array();
		$add_essay_question_result['add_essay_question'] = $add_essay_question;
		$add_essay_question_result['error_code'] = "0";
		$add_essay_question_result['message'] = "essay Question Added Successfully";
		return response()->json($add_essay_question_result);

	}

	/* add demographic type questions*/
	public function add_demography_question(Request $request){

		$question_text = $request->input('question_text');
		$option_one = $request->input('option_one');
		$option_two = $request->input('option_two');
		$option_three = $request->input('option_three');
		$option_four = $request->input('option_four');
		$correct_answer = $request->input('correct_answer');
		$subject_name = $request->input('subject_name');
		$category_id = $request->input('category_id');
		$login_admin_id = $request->input('login_admin_id');
		$login_admin_name = $request->input('login_admin_name');

		$add_demographic_question = new AddQuestion();
		$add_demographic_question->question_text = $question_text;
		$add_demographic_question->option_one  = $option_one;
		$add_demographic_question->option_two  = $option_two;
		$add_demographic_question->option_three  = $option_three;
		$add_demographic_question->option_four  = $option_four;
		$add_demographic_question->correct_answer = $correct_answer;
		$add_demographic_question->subject_name = $subject_name;
		$add_demographic_question->category_id = $category_id;
		$add_demographic_question->login_admin_name = $login_admin_name;
		$add_demographic_question->login_admin_id =$login_admin_id;
		$add_demographic_question->question_type = "demographic_questions";
		$add_demographic_question->save();

		$add_demographic_question_result = array();
		$add_demographic_question_result['add_demographic_question'] = $add_demographic_question;
		$add_demographic_question_result['error_code'] = "0";
		$add_demographic_question_result['message'] = "Demographic Question Added Successfully";
		return response()->json($add_demographic_question_result);

	} 

	/** Listing of questions ** *Starts*/ 

	/* Listing of category of questions*/
	public function question_category_list(Request $request){

		$login_admin_id = $request->input('login_admin_id');

		$get_category_detail = AdminCategory::where('login_admin_id',$login_admin_id)->get();
		$category_detail_list = array();
		$category_detail_list['category_list'] = $get_category_detail;
		$category_detail_list['error_code'] = "0";
		$category_detail_list['message'] = " List of questions category";
		return response()->json($category_detail_list);
	}

	/* listing of subject list with category name*/
	public function subjects_list(Request $request){

		$login_admin_id = $request->input('login_admin_id');

		$subject_list = SubjectDetail::with('category_detail')->where('login_admin_id',$login_admin_id)->get();
		$subject_detail_list = array();
		$subject_detail_list['subject_list'] = $subject_list;
		$subject_detail_list['error_code'] = "0";
		$subject_detail_list['message'] = " List of questions subject";
		return response()->json($subject_detail_list);

	} 

	/* listing of mcqs questions*/
	public function mcqs_list(Request $request){

		$question_type = $request->input('question_type');
		$login_admin_id = $request->input('login_admin_id');

		$get_mcqs_questions_list = AddQuestion::where('question_type',$question_type)->where('login_admin_id',$login_admin_id)->where('del_status','active')->get();
		$get_mcqs_questions_result = array();
		$get_mcqs_questions_result['list_of_mcqs_questions'] = $get_mcqs_questions_list;
		$get_mcqs_questions_result['error_code'] = "0";
		$get_mcqs_questions_result['message'] = 'List of mcqs questions';
		return response()->json($get_mcqs_questions_result);
	}

	/* listing of multiple responce questions*/ 
	public function mrqs_list(Request $request){

		$question_type = $request->input('question_type');
		$login_admin_id = $request->input('login_admin_id');

		$get_mrqs_questions_list = AddQuestion::where('question_type',$question_type)->where('login_admin_id',$login_admin_id)->where('del_status','active')->get();
		$get_mrqs_questions_result = array();
		$get_mrqs_questions_result['list_of_mrqs_questions'] = $get_mrqs_questions_list;
		$get_mrqs_questions_result['error_code'] = "0";
		$get_mrqs_questions_result['message'] = 'List of multiple response questions';
		return response()->json($get_mrqs_questions_result);
	}

	/* listing of true_false*/ 
	public function true_false_list(Request $request){

		$question_type = $request->input('question_type');
		$login_admin_id = $request->input('login_admin_id');

		$get_true_false_questions_list = AddQuestion::where('question_type',$question_type)->where('login_admin_id',$login_admin_id)->where('del_status','active')->get();
		$get_true_false_questions_result = array();
		$get_true_false_questions_result['list_of_true_false_questions'] = $get_true_false_questions_list;
		$get_true_false_questions_result['error_code'] = "0";
		$get_true_false_questions_result['message'] = 'List of true false questions';
		return response()->json($get_true_false_questions_result);
	}

	/* listing short questions*/
	public function short_questions_list(Request $request){

		$question_type = $request->input('question_type');
		$login_admin_id  = $request->input('login_admin_id');
		$get_short_questions_list = AddQuestion::where('question_type',$question_type)->where('login_admin_id',$login_admin_id)->where('del_status','active')->get();
		$get_short_questions_result = array();
		$get_short_questions_result['list_of_short_questions'] = $get_short_questions_list;
		$get_short_questions_result['error_code'] = "0";
		$get_short_questions_result['message'] = 'List of Short questions';
		return response()->json($get_short_questions_result);
	} 

	/* listing of numeric questions*/ 
	public function numeric_questions_list(Request $request){

		$question_type = $request->input('question_type');
		$login_admin_id = $request->input('login_admin_id');
		$get_numeric_questions_list = AddQuestion::where('question_type',$question_type)->where('login_admin_id',$login_admin_id)->where('del_status','active')->get();
		$get_numeric_questions_result = array();
		$get_numeric_questions_result['list_of_numeric_questions'] = $get_numeric_questions_list;
		$get_numeric_questions_result['error_code'] = "0";
		$get_numeric_questions_result['message'] = 'List of numeric questions';
		return response()->json($get_numeric_questions_result);
	} 

	/* listing of essay type questions*/ 
	public function essay_questions_list(Request $request){

		$question_type = $request->input('question_type');
		$login_admin_id = $request->input('login_admin_id');
		$get_essay_questions_list = AddQuestion::where('question_type',$question_type)->where('login_admin_id',$login_admin_id)->where('del_status','active')->get();
		$get_essay_questions_result = array();
		$get_essay_questions_result['list_of_essay_questions'] = $get_essay_questions_list;
		$get_essay_questions_result['error_code'] = "0";
		$get_essay_questions_result['message'] = 'List of essay questions';
		return response()->json($get_essay_questions_result);

	}

	/* listing of demographic questions*/
	public function demographic_questions_list(Request $request){

		$question_type = $request->input('question_type');
		$login_admin_id = $request->input('login_admin_id');
		$get_demographic_questions_list = AddQuestion::where('question_type',$question_type)->where('login_admin_id',$login_admin_id)->where('del_status','active')->get();
		$get_demographic_questions_result = array();
		$get_demographic_questions_result['list_of_demographic_questions'] = $get_demographic_questions_list;
		$get_demographic_questions_result['error_code'] = "0";
		$get_demographic_questions_result['message'] = 'List of demographic questions';
		return response()->json($get_demographic_questions_result);		
	}
	
	/* end of listing all type of questions*/ 

	/*added questions list*/ 
	public function questions_list(Request $request){

		$login_admin_id = $request->input('login_admin_id');

		$get_question_list = AddQuestion::with('category_details','subject_details')->where('login_admin_id',$login_admin_id)->get();
		$media_base_url = url('/');
		$result_get_question_list = array();
		$result_get_question_list['questions_list'] = $get_question_list;
		$result_get_question_list['error_code']  = "0";
		$result_get_question_list['message'] = "List of questions";
		$result_get_question_list['media_base_url'] = $media_base_url;
		return response()->json($result_get_question_list);
	}
//end class			
}
?>