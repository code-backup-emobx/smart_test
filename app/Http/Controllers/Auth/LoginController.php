<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(){

        return view ('/auth/login');
    }

    public function authentication(Request $request)
    {
       // dd(12);

        $validator = Validator::make($request->all(), [
                    
                //'petbreed_name' => 'required|alpha_spaces',
                'email' => 'required',
                'password' => 'required'
                ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $email = $request->email;
            $password = $request->password;

        // $admin_login = Admin::where('admin_email', '=', $email)->
        // where('admin_password', '=', $password)->
        // get();
        

        if (\Auth::attempt(['email' => $email, 'password' => $password, 'user_type' => 'super_admin' , 'status' => '1'])) 
        {
            return redirect('/')->with('success','Welcome Back');
        }
        else
        {
            return redirect()->back()->with('fail','!Enter the valid email and password');
        }
    }

}
