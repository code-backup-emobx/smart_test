<?php

namespace App\Http\Controllers ;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use \App\Model\User;
Use App\Models\AllUsers;
Use App\Models\AdminCategory;
Use App\Models\SubjectDetail;
use Validator;
use DB;

class CategoryController extends Controller
{

    public function __construct(){

        $this->middleware('auth');
    }
    //admin Controller
    public function add_category(){

        return view('/addCategory/add_category');
    }

    public function submit_category(Request $request){

        $validator = Validator::make($request->all(), [
            
        'category_name' => 'required|regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/' 
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $category_name = $request->input('category_name');
        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $save_category = new AdminCategory();

        $save_category->category_name = $category_name;
        $save_category->login_admin_name = $login_admin_name;
        $save_category->login_admin_id = $login_admin_id;
        $save_category->save();

        return redirect('/category_list')->with('success','Category added successfully');
    }

    public function category_list(){

        $login_admin_id = Auth::user()->id;
        $category_list = AdminCategory::where('login_admin_id', $login_admin_id)->where('status','active')->get();
        return view('/addCategory/category_list', compact('category_list'));
    }

    public function edit_category($id){

        $edit_category_detail = AdminCategory::where('category_id',$id)->first(); 
        return view('/addCategory/edit_category', compact('edit_category_detail'));
    }
    
    public function update_category(Request $request , $id){
        // dd(12);die();
        $validator = Validator::make($request->all(), [
            
        'category_name' => 'required|regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/' 
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $category_name = $request->input('category_name');
        $status = $request->input('status');

        $update = AdminCategory::where('category_id',$id)->update(['category_name'=>$category_name, 'status'=> $status]);
        return redirect('/category_list')->with('success','Category Updated Successfully ');
    }

    public function delete_category_detail($id){

        $deletestatement = AdminCategory::where('category_id',$id)->update(['status'=>'inactive']);
        return redirect('/category_list')->with('success','Category deleted Successfully ');

    }
    //add subject 
    public function add_subject(){
        $login_admin_id = Auth::user()->id;
        $category_detail = AdminCategory::where('status','active')->where('login_admin_id',$login_admin_id)->get();
        return view('/addCategory/add_subject',compact('category_detail'));
    }

    //submit subject
    public function submit_subject(Request $request){

        $validator = Validator::make($request->all(), [
            
        'subject_name' => 'required|regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/' 
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $subject_name = $request->input('subject_name');
        $category_name = $request->category_name;
        $duration = $request->input('duration');
        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;

        $submit_subject = new SubjectDetail();
        $submit_subject->subject_name = $subject_name;
        $submit_subject->category_id = $category_name;
        $submit_subject->duration = $duration;
        $submit_subject->login_admin_name = $login_admin_name;
        $submit_subject->login_admin_id = $login_admin_id;

        $submit_subject->save();
        return redirect('/category_list')->with('success','Subject Added Successfully');

    }

    //subjects list
    public function subject_list(){
        $login_admin_id = Auth::user()->id;
        // return $subject_list = SubjectDetail::with('category_detail')->where('login_admin_id',$login_admin_id)->get();die();
        // $subject_list = DB::table('add_subject')
        //     ->join('add_category', 'add_subject.category_id', '=', 'add_category.category_id')
        //     ->where('add_subject.login_admin_id','=',$login_admin_id)
        //     ->get();
        $subject_list =   SubjectDetail::with('category_details')->where('login_admin_id',$login_admin_id)->paginate();
           return view('/addCategory/subject_detail', compact('subject_list'));
    }

    public function edit_subject($id){
        $login_admin_id = Auth::user()->id;
        // $edit_subject =  DB::table('add_subject')
        //     ->join('add_category', 'add_subject.category_id', '=', 'add_category.category_id')
        //     ->where('add_subject.login_admin_id','=',$login_admin_id)
        //     ->where('add_subject.id','=',$id)
        //     ->get();
        $catg_listing = AdminCategory::where('login_admin_id',$login_admin_id)->get();
        $edit_subject =   SubjectDetail::with('category_details')->where('login_admin_id',$login_admin_id)->where('id',$id)->first();
        return view('/addCategory/edit_subject', compact('edit_subject','catg_listing'));
    }

    //update subject 
    public function update_subject(Request $request , $id){

        $validator = Validator::make($request->all(), [
            
        'subject_name' => 'required|regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/' 
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $subject_name = $request->input('subject_name');
        $category_name = $request->category_name;
        $duration = $request->input('duration');
        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;

        $submit_subject = SubjectDetail::find($id);
        $submit_subject->subject_name = $subject_name;
        $submit_subject->category_id = $category_name;
        $submit_subject->duration = $duration;
        $submit_subject->login_admin_name = $login_admin_name;
        $submit_subject->login_admin_id = $login_admin_id;

        $submit_subject->save();
        return redirect('/subject_list')->with('success','Subject updated successfully');

    }

  
    
//end class   
}