<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AllUsers;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $login_admin_id = Auth::user()->id;
        $login_detail = AllUsers::where('user_type','super_admin')->where('id',$login_admin_id)->first();
        $all_admins_count = AllUsers::where('user_type','admin')->where('status','1')->count();
        return view('/superadmin_dashboard',compact('all_admins_count','login_detail'));
    }

    public function admin(){

        $login_admin_id = Auth::user()->id;
        $login_admin_detail = AllUsers::where('user_type','admin')->where('id',$login_admin_id)->first();
        return view('/admin_dashboard',compact('login_admin_detail'));
    }
    public function settings()
    {
        return view('admin/settings');
    }
    public function setting(){
        return view('addCategory/setting');
    }
    public function student(){
        return view('/student_dashboard');
    }
}
