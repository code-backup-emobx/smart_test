<?php

namespace App\Http\Controllers ;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use \App\Model\User;
Use App\Models\AllUsers;
Use App\Models\AdminCategory;
Use App\Models\SubjectDetail;
Use App\Models\AddQuestion;
Use App\Models\Institutes;
Use App\Models\InstituteStudents;
use Validator;
use DB;

class InstitutestudentController extends Controller
{

    public function __construct(){

        $this->middleware('auth');
    }

    public function add_institute_students(){

    	return view('/institutestudent/add_institutes');

    }

    //submit institute
    public function submit_institute(Request $request){

    	$login_admin_id = Auth::user()->id;
    	$login_admin_name = Auth::user()->name;
    	$institute_name = $request->input('institute_name');
    	$add_total_students = "10";

    	if( $test = Institutes::where(["institute_name" => $institute_name],["login_admin_id" => $login_admin_id])->first()){
    		Institutes::where('id', $test->id)->update(array(
                'institute_name' => $institute_name
            ));
    	}
    	else{
	    	$submit_institute = new Institutes();
	    	$submit_institute->institute_name = $institute_name;
	    	$submit_institute->login_admin_id = $login_admin_id;
	    	$submit_institute->login_admin_name = $login_admin_name;
	    	$submit_institute->add_total_students = $add_total_students;
	    	$submit_institute->save();
    	}
    	return redirect('/institutes_list')->with('success','Institute Added Successfully');
    }

    //listing institutes
    public function institutes_list(){

    	$institutes_list = Institutes::where('status','active')->get();
    	return view('/institutestudent/institutes_list',compact('institutes_list'));
    }

    public function edit_institute($id){

    	$edit_institute = Institutes::where('id',$id)->first();
    	return view('/institutestudent/edit_institutes',compact('edit_institute'));

    }

    public function update_institute(Request $request , $id){

    	$institute_name = $request->input('institute_name');

    	$update_institute = Institutes::find($id);
    	$update_institute->institute_name = $institute_name;
    	$update_institute->save();

    	return redirect('/institutes_list')->with('success','Institute Updated Successfully');
    }

    public function delete_institute($id){

		$delete_institute = Institutes::find($id);
		$delete_institute->status = "inactive";
        $delete_institute->save();
        return redirect('institutes_list')->with('success','Institute Deleted Successfully');    	
    }

    //institute add students

    public function add_students(){
    	$login_admin_name = Auth::user()->name;
    	$login_admin_id = Auth::user()->id;

    	$institute_list = Institutes::where('login_admin_id',$login_admin_id)->get();
    	return view('/institutestudent/add_students',compact('institute_list'));
    }
    //submit students
    public function submit_students(Request $request){

        $validator = Validator::make($request->all(), [
        'student_phone_number' => 'required|max:10|min:10',
        'email' => 'unique:user' 
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }
		$login_admin_name = Auth::user()->name;
    	$login_admin_id = Auth::user()->id;

    	$institute_name = $request->institute_name;
    	$student_name = $request->input('student_name');
        $student_email = $request->input('student_email');
        $student_password = $request->input('student_password');
        $student_phone_number = $request->input('student_phone_number');

    	//student count decrease
    	
    	 $institute_id = $institute_name;
        $students_total_number = Institutes::find($institute_id);

        if($students_total_number->add_total_students != 0){

            $submit_institute = new AllUsers();
            $submit_institute->name = $student_name;
            $submit_institute->email = $student_email;
            $submit_institute->password = Hash::make($student_password);
            $submit_institute->admin_password = $student_password;
            $submit_institute->phone_no = $student_phone_number;
            $submit_institute->user_type = "institute_student";
            $submit_institute->login_admin_id = $login_admin_id;
            $submit_institute->login_admin_name = $login_admin_name;
            $submit_institute->save();

            $student_id = $submit_institute->id;
            $save_institute_student = new InstituteStudents();
            $save_institute_student->institute_name = $institute_name;
            $save_institute_student->student_name = $student_id;
            $save_institute_student->login_admin_id = $login_admin_id;
            $save_institute_student->login_admin_name = $login_admin_name;
            $save_institute_student->save();

            $remaining_institute_students = $students_total_number->add_total_students - (InstituteStudents::where('institute_name', $institute_name)->where('login_admin_id',$login_admin_id)->count());
           

            $save_remaining_students = Institutes::find($institute_id);
            $save_remaining_students->add_total_students = $remaining_institute_students;
            $save_remaining_students->save();
          
        }
        else
        {
            return back()->with('fail','you reach the value');
        }

    	return back()->with('success','Student Added Successfully');    	
    }

    public function students_list(){

        $login_admin_id = Auth::user()->id;
        $login_admin_name = Auth::user()->name;

        $get_students_list = InstituteStudents::with('institute_details','student_details')->where('login_admin_id',$login_admin_id)->where('student_status','active')->where('student_status','active')->paginate();

        return view('/institutestudent/students_list',compact('get_students_list'));
    }
// end class
}    