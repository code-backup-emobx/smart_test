<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Session;
use Validator;
use App\Models\AllUsers;
use App\User;
use App\Models\InstituteStudents;
use DB;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    // public function authenticate()
    // {
    //     if (Auth::attempt(['email' => $email, 'password' => $password])) {
    //         // Authentication passed...
    //         return redirect()->intended('dashboard');
    //     }
    // }
     public function authenticate(){

        return view ('/auth/login');
    }
    public function authentication(Request $request)
    {
       // dd(12);

        $validator = Validator::make($request->all(), [
                    
                'email' => 'required',
                'password' => 'required'
                ]);

            if ($validator->fails()) 
            {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $email = $request->email;
            $password = $request->password;


        // $admin_login = Admin::where('admin_email', '=', $email)->
        // where('admin_password', '=', $password)->
        // get();
        

        if (\Auth::attempt(['email' => $email, 'password' => $password, 'user_type' => 'super_admin' , 'status' => '1'])) 
        {
            return redirect('/')->with('success','Welcome Back');
        }
       
        if (\Auth::attempt(['email' => $email, 'password' => $password, 'user_type' => 'admin' , 'status' => '1'])) 
        {
            return redirect('/admin_dashboard')->with('success','Welcome Back');
        }
        if (\Auth::attempt(['email' => $email, 'password' => $password, 'user_type' => 'institute_student' , 'status' => '1'])) 
        {
            return redirect('/student_dashboard')->with('success','Welcome Back');
        }
        else
        {
            return redirect()->back()->with('fail','!Enter the valid email and password');
        }
        

    }

}

?>