<?php

namespace App\Http\Controllers ;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use \App\Model\User;
Use App\Models\AllUsers;
Use App\Models\AdminCategory;
Use App\Models\SubjectDetail;
Use App\Models\AddQuestion;
Use App\Models\Institutes;
use Validator;
use DB;

class QuestionController extends Controller
{

    public function __construct(){

        $this->middleware('auth');
    }
    
    public function add_questions()
    {
        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $institute_detail = Institutes::where('login_admin_id',$login_admin_id)->get();
        $get_subject_name = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $category_name = AdminCategory::where('login_admin_id',$login_admin_id)->get();
        return view('/addQuestions/add_questions',compact('get_subject_name','category_name','institute_detail'));
    }

    //add MCQ'S question 
    public function submit_questions(Request $request){
        // dd(12);die();
       
        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $option_three = $request->input('option_three');
        $option_four = $request->input('option_four');
        $correct_answer = $request->input('correct_answer');
        $duration = $request->input('duration');
        $institute_name = $request->institute_name;

        $question_type = "mcqs";

        $add_questions = DB::table('add_questions')->insert(['question_text' => $question_text, 'subject_name' => $subject_name, 'category_id' => $category_name, 'login_admin_name' => $login_admin_name , 'login_admin_id' => $login_admin_id , 'question_type' => $question_type ,'option_one'=> $option_one,'option_two'=>$option_two, 'option_three'=>$option_three,'option_four' => $option_four,'correct_answer' => $correct_answer , 'duration' => $duration , 'institute_name' => $institute_name ]);
        return redirect('/mcqs_qs_list')->with('success','Successfully Added');
    }

    //listing of mcqs Questions
    public function mcqs_qs_list(){

        $login_admin_id = Auth::user()->id;
        $mcqs_question_list =   AddQuestion::with('subject_details','category_details')->where('login_admin_id',$login_admin_id)->where('question_type','mcqs')->where('del_status','active')->paginate(10);   
        return view('/addQuestions/mcqs_qs_list',compact('mcqs_question_list'));
    }
    //edit mcqs question 
    public function edit_mcqs_qs($id){

        $login_admin_id = Auth::user()->id;
        $subject_listing = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $catg_listing = AdminCategory::where('login_admin_id',$login_admin_id)->get();
        $update_question = AddQuestion::where('login_admin_id',$login_admin_id)->where('question_id',$id)->where('question_type','mcqs')->first();
        return view('/addQuestions/edit_mcqs_qs',compact('catg_listing','update_question','subject_listing'));
    }

    //update question
    public function update_mcqs_qs(Request $request , $id){

        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $option_three = $request->input('option_three');
        $option_four = $request->input('option_four');
        $correct_answer = $request->input('correct_answer');

        $update_question = AddQuestion::find($id);
        $update_question->subject_name = $subject_name;
        $update_question->category_id = $category_name;
        $update_question->question_text = $question_text;
        $update_question->option_one = $option_one;
        $update_question->option_two = $option_two;
        $update_question->option_three = $option_three;
        $update_question->option_four = $option_four;
        $update_question->correct_answer = $correct_answer;
        $update_question->save();
        return redirect('/mcqs_qs_list')->with('success','MCQS Question Updated Successfully');
    }

    //delete Question 
    public function delete_question($id){

        $delete_question = AddQuestion::find($id);
        $delete_question->del_status = "inactive";
        $delete_question->save();
        return redirect('mcqs_qs_list')->with('success','MCQS Question Deleted Successfully');
    }

    //add Multiple responce qs
    public function add_multiple_responce_qs(){

        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $institute_detail = Institutes::where('login_admin_id',$login_admin_id)->get();
        $get_subject_name = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $category_name = AdminCategory::where('login_admin_id',$login_admin_id)->get();
       
        return view('/addQuestions/add_multiple_response_qs' , compact('get_subject_name','category_name','institute_detail'));
    }

    //submit Multiple responce qs
    public function submit_multiple_responce_qs( Request $request){

        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $option_three = $request->input('option_three');
        $option_four = $request->input('option_four');
        $correct_answer = $request->input('correct_answer');
        $duration = $request->input('duration');
        $institute_name = $request->institute_name;

        $question_type = "mrqs";

        $add_questions = DB::table('add_questions')->insert(['question_text' => $question_text, 'subject_name' => $subject_name, 'category_id' => $category_name, 'login_admin_name' => $login_admin_name , 'login_admin_id' => $login_admin_id , 'question_type' => $question_type , 'option_one' => $option_one , 'option_two' => $option_two , 'option_three' => $option_three , 'option_four' => $option_four ,'correct_answer'=>$correct_answer , 'duration' => $duration, 'institute_name' => $institute_name]);

        return redirect('/multiple_response_qs_list')->with('success','Multiple Responce Question Added Successfully');
    }

    //listing Multiple responce qs
    public function multiple_response_qs_list(){
        $login_admin_id = Auth::user()->id;
        $mrqs_question_list =   AddQuestion::with('subject_details','category_details')->where('login_admin_id','=',$login_admin_id)->where('question_type','mrqs')->where('del_status','active')->paginate(10); 
        return view('/addQuestions/multiple_response_qs_list',compact('mrqs_question_list'));
    }

    //edit Multiple response question
    public function edit_mrqs($id){

        $login_admin_id = Auth::user()->id;
        $subject_listing = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $catg_listing = AdminCategory::where('login_admin_id',$login_admin_id)->get();
        $mrqs_question = AddQuestion::where('login_admin_id',$login_admin_id)->where('question_id',$id)->where('question_type','mrqs')->first();
        return view('/addQuestions/edit_mrqs',compact('subject_listing','catg_listing','mrqs_question'));
    }

    //update MRQS Questions
    public function update_mrqs(Request $request, $id){

        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $option_three = $request->input('option_three');
        $option_four = $request->input('option_four');
        $correct_answer = $request->input('correct_answer');

        $update_mrqs = AddQuestion::find($id);
        $update_mrqs->subject_name = $subject_name;
        $update_mrqs->category_id = $category_name;
        $update_mrqs->question_text = $question_text;
        $update_mrqs->option_one = $option_one;
        $update_mrqs->option_two = $option_two;
        $update_mrqs->option_three = $option_three;
        $update_mrqs->option_four = $option_four;
        $update_mrqs->correct_answer = $correct_answer;
        $update_mrqs->save();

        return redirect('/multiple_response_qs_list')->with('success','Multiple Response Question Updated Successfully');
    }

    //delete MRQS Question
    public function delete_mrqs_question( $id ){

        $delete_mrqs_question = AddQuestion::find($id);
        $delete_mrqs_question->del_status = "inactive";
        $delete_mrqs_question->save();
        return redirect('/multiple_response_qs_list')->with('success','MRQS Question Deleted Successfully');
    }

    //add True False

    public function add_true_false(){

        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $institute_detail = Institutes::where('login_admin_id',$login_admin_id)->get();
        $get_subject_name = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $category_name = AdminCategory::where('login_admin_id',$login_admin_id)->get();
       
        return view('/addQuestions/add_true_false',compact('get_subject_name','category_name','institute_detail'));
    }

    public function submit_add_true_false(Request $request){

        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');

        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $correct_answer = $request->input('correct_answer');
        $institute_name = $request->input('institute_name');
        $question_type = "true_false";

        $add_questions = DB::table('add_questions')->insert(['question_text' => $question_text, 'subject_name' => $subject_name, 'category_id' => $category_name, 'login_admin_name' => $login_admin_name , 'login_admin_id' => $login_admin_id , 'question_type' => $question_type,'option_one'=> $option_one , 'option_two' =>$option_two,'correct_answer'=> $correct_answer ,'institute_name' => $institute_name]);

        return redirect('/true_false_list')->with('success','Successfully Added True False Question');
    }

    //listing
    public function true_false_list(){

        $login_admin_id = Auth::user()->id;
        $true_false_question_list =   AddQuestion::with('subject_details','category_details')->where('login_admin_id','=',$login_admin_id)->where('question_type','true_false')->where('del_status','active')->paginate(10); 
        return view('/addQuestions/true_false_list',compact('true_false_question_list'));
    }

    //edit true false 
    public function edit_true_false($id){

        $login_admin_id = Auth::user()->id;
        $subject_listing = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $catg_listing = AdminCategory::where('login_admin_id',$login_admin_id)->get();

        $edit_true_false_question = AddQuestion::where('login_admin_id',$login_admin_id)->where('question_id',$id)->where('question_type','true_false')->where('del_status','active')->first();
       
        return view('/addQuestions/edit_true_false',compact('subject_listing','catg_listing','edit_true_false_question'));
    }

    //update true false
    public function update_true_false(Request $request , $id){

        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $correct_answer = $request->input('correct_answer');

        $update_true_false = AddQuestion::find($id);
        $update_true_false->subject_name = $subject_name;
        $update_true_false->category_id = $category_name;
        $update_true_false->question_text = $question_text;
        $update_true_false->option_one = $option_one;
        $update_true_false->option_two = $option_two;
        $update_true_false->correct_answer = $correct_answer;
        $update_true_false->save();

        return redirect('/true_false_list')->with('success','True False Statement Updated Successfully');
  
    }

    //delete true false 
    public function delete_true_false($id){

        $delete_true_false = AddQuestion::find($id);
        $delete_true_false->del_status = "inactive";
        $delete_true_false->save();
        return redirect('/true_false_list')->with('success','true Flase Statement Updated Successfully');
    }

    //add short answer type questions
    public function add_short_questions(){

        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $institute_detail = Institutes::where('login_admin_id',$login_admin_id)->get();
        $get_subject_name = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $category_name = AdminCategory::where('login_admin_id',$login_admin_id)->get();
       
        return view('/addQuestions/add_short_questions',compact('get_subject_name','category_name','institute_detail'));
    }

    public function submit_short_questions( Request $request){

        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');
        $duration = $request->input('duration');
        $institute_name = $request->institute_name;

        $question_type = "short_answer";

        $add_questions = DB::table('add_questions')->insert(['question_text' => $question_text, 'subject_name' => $subject_name, 'category_id' => $category_name, 'login_admin_name' => $login_admin_name , 'login_admin_id' => $login_admin_id , 'question_type' => $question_type,'duration'=> $duration , 'institute_name' => $institute_name ]);
        return redirect('/short_question_list');
    }

    public function short_question_list(){

        $login_admin_id = Auth::user()->id;
        $short_question_list = AddQuestion::with('subject_details','category_details')->where('login_admin_id','=',$login_admin_id)->where('question_type','short_answer')->where('del_status','active')->paginate(10); 
       
        return view('/addQuestions/short_question_list',compact('short_question_list'));
    }

    //edit short_answer_question
    public function edit_short_answer_question($id){

        $login_admin_id = Auth::user()->id;
        $subject_listing = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $catg_listing = AdminCategory::where('login_admin_id',$login_admin_id)->get();

        $edit_short_question = AddQuestion::where('login_admin_id',$login_admin_id)->where('question_id',$id)->where('question_type','short_answer')->where('del_status','active')->first();
        return view('/addQuestions/edit_short_qs',compact('subject_listing','catg_listing','edit_short_question'));
    }

    //update short Answer
    public function update_short_answer(Request $redirect , $id){

        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');

        $update_short_answer = AddQuestion::find($id);
        $update_short_answer->subject_name = $subject_name;
        $update_short_answer->category_id = $category_name;
        $update_short_answer->question_text = $question_text;
        $update_short_answer->save();
        return redirect('/short_question_list')->with('success','Short Question Statement Update Successfully');
    }

    //delete Short Answer
    public function delete_short_question($id){

        $delete_short_question = AddQuestion::find($id);
        $delete_short_question->del_status = 'inactive';
        $delete_short_question->save();
        return redirect('short_question_list')->with('success','Short Question Deleted Successfully');
    }
    
    //add numeric questions
    public function add_numeric_questions(){

        $login_admin_id = Auth::user()->id;
        $institute_detail = Institutes::where('login_admin_id',$login_admin_id)->get();
        $get_subject_name = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $category_name = AdminCategory::where('login_admin_id',$login_admin_id)->get();
        return view('/addQuestions/numeric_questions',compact('get_subject_name','category_name','institute_detail'));
    }

    //submit numeric question 
    public function submit_numeric_questions(Request $request){

        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $numeric_picture = $request->input('numeric_picture');
        $question_text = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $option_three = $request->input('option_three');
        $option_four = $request->input('option_four');
        $correct_answer = $request->input('correct_answer');
        $duration = $request->input('duration');
        $institute_name = $request->institute_name;

        if ($request->hasFile('numeric_picture') != "") {
            $image = $request->file('numeric_picture');
            // $filename = time() . '.' . $image->getClientOriginalExtension();
            $filename =$image->getClientOriginalName();
            $destinationPath = public_path('/numeric_pictures');
            $image->move($destinationPath, $filename);
            $numeric_picture = '/numeric_pictures/' . $filename;
        } else {
            $numeric_picture = "";
        }
        $add_numeric_questions = new AddQuestion();
        $add_numeric_questions->subject_name = $subject_name;
        $add_numeric_questions->category_id = $category_name;
        $add_numeric_questions->numeric_picture = $numeric_picture;
        $add_numeric_questions->question_text = $question_text;
        $add_numeric_questions->option_one = $option_one;
        $add_numeric_questions->option_two = $option_two;
        $add_numeric_questions->option_three = $option_three;
        $add_numeric_questions->option_four = $option_four;
        $add_numeric_questions->correct_answer = $correct_answer;
        $add_numeric_questions->duration = $duration;
        $add_numeric_questions->institute_name = $institute_name;



        $add_numeric_questions->login_admin_id = $login_admin_id;
        $add_numeric_questions->login_admin_name = $login_admin_name;
        $add_numeric_questions->question_type = "numeric_question";
        $add_numeric_questions->save();
        return redirect('/numeric_questions_list')->with('success','Numeric Question Added Successfully');
    }

    //list of numeric list
    public function numeric_questions_list(){

        $login_admin_id = Auth::user()->id;
        $numeric_questions_list =   AddQuestion::with('subject_details','category_details')->where('login_admin_id','=',$login_admin_id)->where('question_type','numeric_question')->where('del_status','active')->paginate(10);
        return view('/addQuestions/numeric_questions_list',compact('numeric_questions_list'));
    }

    //edit numeric question 
    public function edit_numeric_question( $id ){

        $login_admin_id = Auth::user()->id;
        $subject_listing = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $catg_listing = AdminCategory::where('login_admin_id',$login_admin_id)->get();

        $edit_numeric_question = AddQuestion::where('login_admin_id',$login_admin_id)->where('question_id',$id)->where('question_type','numeric_question')->where('del_status','active')->first();
       
        return view('/addQuestions/edit_numeric_question',compact('edit_numeric_question','subject_listing','catg_listing'));
    }

    //update question
    public function update_numeric_question(Request $request , $id){

        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;
        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $numeric_picture = $request->input('numeric_picture');
        $question_text = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $option_three = $request->input('option_three');
        $option_four = $request->input('option_four');
        $correct_answer = $request->input('correct_answer');

        if ($request->hasFile('numeric_picture') != "") {
            $image = $request->file('numeric_picture');
            // $filename = time() . '.' . $image->getClientOriginalExtension();
            $filename =$image->getClientOriginalName();
            $destinationPath = public_path('/numeric_pictures');
            $image->move($destinationPath, $filename);
            $numeric_picture = '/numeric_pictures/' . $filename;

            $add_numeric_questions = AddQuestion::find($id);
            $add_numeric_questions->subject_name = $subject_name;
            $add_numeric_questions->category_id = $category_name;
            $add_numeric_questions->numeric_picture = $numeric_picture;
            $add_numeric_questions->question_text = $question_text;
            $add_numeric_questions->option_one = $option_one;
            $add_numeric_questions->option_two = $option_two;
            $add_numeric_questions->option_three = $option_three;
            $add_numeric_questions->option_four = $option_four;
            $add_numeric_questions->correct_answer = $correct_answer;
            $add_numeric_questions->login_admin_id = $login_admin_id;
            $add_numeric_questions->login_admin_name = $login_admin_name;
            $add_numeric_questions->question_type = "numeric_question";
            $add_numeric_questions->save();

        } else {
            $numeric_picture = "";
        }
        

        return redirect('/numeric_questions_list')->with('success','Numeric Question Updated Successfully');

    }
    //delete numeric question
    public function delete_numeric_question($id){

        $delete_numeric_question = AddQuestion::find($id);
        $delete_numeric_question->del_status = "inactive";
        $delete_numeric_question->save();
        return redirect('/numeric_questions_list')->with('success','Numeric Question Deleted Successfully');
    }

    //add essay questions
    public function add_essay_question(){

        $login_admin_id = Auth::user()->id;
        $institute_detail = Institutes::where('login_admin_id',$login_admin_id)->get();
        $get_subject_name = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $category_name = AdminCategory::where('login_admin_id',$login_admin_id)->get();
        return view('/addQuestions/add_essay_qs',compact('get_subject_name','category_name'));

    }

    //submit essay question
    public function submit_essay_questions( Request $request ){
       
        $login_admin_name = Auth::user()->name; 
        $login_admin_id = Auth::user()->id;

        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');
        $institute_name = $request->institute_name;

        $duration = $request->input('duration');

        $submit_essay_question = new AddQuestion();
        $submit_essay_question->subject_name = $subject_name;
        $submit_essay_question->category_id  = $category_name;
        $submit_essay_question->question_text = $question_text;
        $submit_essay_question->login_admin_id = $login_admin_id;
        $submit_essay_question->login_admin_name = $login_admin_name;
        $submit_essay_question->duration = $duration;
        $submit_essay_question->institute_name = $institute_name;
        $submit_essay_question->question_type = "essay_question";
        $submit_essay_question->save();
        return view('/essay_qs_list')->with('success','Essay Question Added Successfuly');
    }

    //listing essay questions
    public function essay_qs_list(){

        $login_admin_id = Auth::user()->id;
        $essay_questions_list =   AddQuestion::with('subject_details','category_details')->where('login_admin_id','=',$login_admin_id)->where('question_type','essay_question')->where('del_status','active')->paginate(10);
        return view('/addQuestions/essay_qs_list',compact('essay_questions_list'));

    }

    public function edit_essay_qs($id){

        $login_admin_id = Auth::user()->id;
        $subject_listing = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $catg_listing = AdminCategory::where('login_admin_id',$login_admin_id)->get();

        $edit_essay_question = AddQuestion::where('login_admin_id',$login_admin_id)->where('question_id',$id)->where('question_type','essay_question')->where('del_status','active')->first();
        return view('/addQuestions/edit_essay_qs',compact('edit_essay_question','subject_listing','catg_listing'));
    }
    //update essay questions
    public function update_essay_qs(Request $request ,$id){

        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text = $request->input('question_text');

        $update_essay_question = AddQuestion::find($id);
        $update_essay_question->subject_name = $subject_name;
        $update_essay_question->category_id = $category_name;
        $update_essay_question->question_text = $question_text;
        $update_essay_question->save();
        return redirect('/essay_qs_list')->with('success','Essay Question Updated Successfully');
    }

    //delete esssay questions
    public function delete_essay_question($id){

        $delete_essay_question = AddQuestion::find($id);
        $delete_essay_question->del_status = "inactive";
        $delete_essay_question->save();
        return redirect('/essay_qs_list')->with('success','Essay Question Statement Deleted Sussfully');
    }

    //add demographic questions
    public function add_demographic_questions(){

        $login_admin_id  = Auth::user()->id;
        $institute_detail = Institutes::where('login_admin_id',$login_admin_id)->get();
        $get_subject_name = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $category_name = AdminCategory::where('login_admin_id',$login_admin_id)->get();
        return view('/addQuestions/demographic_questions',compact('get_subject_name','category_name'));
    }

    //submit the demographic questions
    public function submit_demographic_questions(Request $request){

        $login_admin_name = Auth::user()->name;
        $login_admin_id = Auth::user()->id;

        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text  = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $option_three = $request->input('option_three');
        $option_four = $request->input('option_four');
        $correct_answer = $request->input('correct_answer');
        $duration = $request->input('duration');
        $institute_name = $request->institute_name;


        $question_type = "demographic_questions";

        $add_demographic_question =  new AddQuestion();
        $add_demographic_question->subject_name = $subject_name;
        $add_demographic_question->category_id = $category_name;
        $add_demographic_question->question_text = $question_text;
        $add_demographic_question->option_one = $option_one;
        $add_demographic_question->option_two = $option_two;
        $add_demographic_question->option_three = $option_three;
        $add_demographic_question->option_four = $option_four;
        $add_demographic_question->correct_answer = $correct_answer;
        $add_demographic_question->duration = $duration;
        $add_demographic_question->institute_name = $institute_name;

        $add_demographic_question->question_type = $question_type;
        $add_demographic_question->login_admin_name = $login_admin_name;
        $add_demographic_question->login_admin_id = $login_admin_id;
        $add_demographic_question->save();

        return redirect('/demographic_questions_list')->with('success','Demographic Question Added Successfully');
    }

    //listing of demographic questions
    public function demographic_questions_list(){

        $login_admin_id = Auth::user()->id;
        $demographic_question_list =   AddQuestion::with('subject_details','category_details')->where('login_admin_id','=',$login_admin_id)->where('question_type','demographic_questions')->where('del_status','active')->paginate(10);
        return view('/addQuestions/demographic_questions_list' , compact('demographic_question_list'));
    }

    //edit demographic questions
    public function edit_demographic_qs($id){

        $login_admin_id = Auth::user()->id;
        $subject_listing = SubjectDetail::where('login_admin_id',$login_admin_id)->get();
        $catg_listing = AdminCategory::where('login_admin_id',$login_admin_id)->get();
        $update_demographic_question = AddQuestion::where('login_admin_id',$login_admin_id)->where('question_id',$id)->where('question_type','demographic_questions')->first();
        return view('/addQuestions/edit_demographic_qs',compact('subject_listing','catg_listing','update_demographic_question'));
    }

    //update demographic questions
    public function update_demographic_qs(Request $request , $id){

        $login_admin_name = Auth::user()->name;
        $login_admin_id = Auth::user()->id;

        $subject_name = $request->subject_name;
        $category_name = $request->category_name;
        $question_text  = $request->input('question_text');
        $option_one = $request->input('option_one');
        $option_two = $request->input('option_two');
        $option_three = $request->input('option_three');
        $option_four = $request->input('option_four');
        $correct_answer = $request->input('correct_answer');
        $question_type = "demographic_questions";

        $add_demographic_question =  AddQuestion::find($id);
        $add_demographic_question->subject_name = $subject_name;
        $add_demographic_question->category_id = $category_name;
        $add_demographic_question->question_text = $question_text;
        $add_demographic_question->option_one = $option_one;
        $add_demographic_question->option_two = $option_two;
        $add_demographic_question->option_three = $option_three;
        $add_demographic_question->option_four = $option_four;
        $add_demographic_question->correct_answer = $correct_answer;
        $add_demographic_question->question_type = $question_type;
        $add_demographic_question->login_admin_name = $login_admin_name;
        $add_demographic_question->login_admin_id = $login_admin_id;
        $add_demographic_question->save();

        return redirect('/demographic_questions_list')->with('success','Demographic Question Updated Successfully');
    }

    //delete demographic question
    public function delete_demographic_qs($id){

        $delete_demographic_qs = AddQuestion::find($id);
        $delete_demographic_qs->del_status = "inactive";
        $delete_demographic_qs->save();
        return redirect('/demographic_questions_list')->with('success','Demographic Question Successfully Delete');
    }
//end class   
}