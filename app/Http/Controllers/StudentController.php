<?php

namespace App\Http\Controllers ;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

Use App\Models\AllUsers;
Use App\Models\AdminCategory;
Use App\Models\SubjectDetail;
Use App\Models\AddQuestion;
Use App\Models\StudentAddQuestionForTest;
use Validator;
use DB;

class StudentController extends Controller
{

	public function create_test(){

		return view('/studentpublic/create_test');
	}

	public function submit_created_test(Request $request){

		$choose_test_type = $request->input('choose_test_type');

		if($choose_test_type == 'public'){
			return view('/studentpublic/public');
		}
		elseif($choose_test_type == 'private'){
			return view('/studentprivate/private');
		}
		else{
			return redirect()->back()->with('fail','Please choose the type of test');
		}
	}

	public function view_addstudent(){
		return view('/studentpublic/create_test_detail');
	}

	public function add_username(Request $request){

		$login_admin_id = Auth::user()->id;
		$login_admin_name = Auth::user()->name;
		$nameofuser = $request->input('name');
		
		$check_login = AllUsers::where('name',$nameofuser)->first();
		if(!empty($check_login)){
			return view('/studentpublic/create_test_detail');
		}
		else{
			return redirect()->back()->with('fail','Please check your credentials first');
		}
		
	}

	public function choose_type_of_questions(Request $request){

		$status = $request->input('status');
		$get_question = AddQuestion::with('subject_details','category_details','institute_details')->where('question_type',$status)->get();
		$get_all_questions = AddQuestion::with('subject_details','category_details','institute_details')->get();
		if($status =='mcqs' || $status =='mrqs' || $status == 'true_false' || $status == 'short_answer' || $status =='numeric_question' || $status == 'demographic_questions' || $status == 'essay_question'){
        	$data = $get_question;
	    }
	    else {
	      $data=$get_all_questions;
	    }
		return view('/studentpublic/create_test_detail',compact('data','status'));
	}    

	//add questions for test
	public function add_question_for_test($id){

		$question_detail = AddQuestion::with('subject_details','category_details','institute_details')->where('question_id',$id)->first();
		
		return view('/studentpublic/add_questions_for_test',compact('question_detail'));
	}

	public function submit_choose_questions_for_test(Request $request, $id){

		$student_login_id = Auth::user()->id;
		$student_login_name = Auth::user()->name;
		$institute_name = $request->input('institute_name');
		$subject_name = $request->input('subject_name');
		$category_name = $request->input('category_name');
		$question_name = $request->input('question_name');
		$option_one = $request->input('option_one');
		$option_two = $request->input('option_two');
		$option_three = $request->input('option_three');
		$option_four = $request->input('option_four');
		$correct_answer = $request->input('option_four');
		$duration = $request->input('duration');

		$add_question = new StudentAddQuestionForTest();
		$add_question->student_login_id = $student_login_id;
		$add_question->student_login_name = $student_login_name;
		$add_question->institute_name = $institute_name;
		$add_question->subject_name = $subject_name;
		$add_question->category_name = $category_name;
		$add_question->question_text = $question_name;
		$add_question->option_one = $option_one;
		$add_question->option_two = $option_two;
		$add_question->option_three = $option_three;
		$add_question->option_four = $option_four;
		$add_question->correct_answer = $correct_answer;
		$add_question->duration = $duration;

		$add_question->test_type = 'public';
		$add_question->save();

		return back()->with('success','Question Added Successfully');
	}

	/* for private students*/
	public function add_detail_for_private_test(Request $request){

		$login_admin_id = Auth::user()->id;
		$login_admin_name = Auth::user()->name;
		$nameofuser = $request->input('name');
		$password = $request->input('password');

		
		$check_login = AllUsers::where('name',$nameofuser)->where('admin_password',$password)->first();
		if(!empty($check_login)){
			return view('/studentprivate/add_test_questions');
		}
		else{
			return redirect()->back()->with('fail','Please check your credentials first');
		}
	}

	public function view_add_test(Request $request){

		$status = $request->input('status');
		$get_question = AddQuestion::with('subject_details','category_details','institute_details')->where('question_type',$status)->get();
		$get_all_questions = AddQuestion::with('subject_details','category_details','institute_details')->get();
		if($status =='mcqs' || $status =='mrqs' || $status == 'true_false' || $status == 'short_answer' || $status =='numeric_question' || $status == 'demographic_questions' || $status == 'essay_question'){
        	$data = $get_question;
	    }
	    else {
	      $data=$get_all_questions;
	    }
		return view('/studentprivate/add_test_questions',compact('data','status'));
	} 

	public function selected_questions_add($id){
		$question_of_detail = AddQuestion::with('subject_details','category_details','institute_details')->where('question_id',$id)->first();
		return view('/studentprivate/selected_questions_add',compact('question_of_detail'));
	}

	public function submit_selected_questions_add(Request $request, $id){

		$student_login_id = Auth::user()->id;
		$student_login_name = Auth::user()->name;
		$institute_name = $request->input('institute_name');
		$subject_name = $request->input('subject_name');
		$category_name = $request->input('category_name');
		$question_name = $request->input('question_name');
		$option_one = $request->input('option_one');
		$option_two = $request->input('option_two');
		$option_three = $request->input('option_three');
		$option_four = $request->input('option_four');
		$correct_answer = $request->input('option_four');
		$duration = $request->input('duration');

		$add_selected_question = new StudentAddQuestionForTest();
		$add_selected_question->student_login_id = $student_login_id;
		$add_selected_question->student_login_name = $student_login_name;
		$add_selected_question->institute_name = $institute_name;
		$add_selected_question->subject_name = $subject_name;
		$add_selected_question->category_name = $category_name;
		$add_selected_question->question_text = $question_name;
		$add_selected_question->option_one = $option_one;
		$add_selected_question->option_two = $option_two;
		$add_selected_question->option_three = $option_three;
		$add_selected_question->option_four = $option_four;
		$add_selected_question->correct_answer = $correct_answer;
		$add_selected_question->duration = $duration;

		$add_selected_question->test_type = 'private';
		$add_selected_question->save();

		return back()->with('success','Question Added Successfully');
	}
//end class   
}