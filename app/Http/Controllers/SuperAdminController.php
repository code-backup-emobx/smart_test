<?php

namespace App\Http\Controllers ;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use \App\Model\User;
Use App\Models\AllUsers;
Use App\Models\AdminCategory;
Use App\Models\AddQuestion;
use DB;
use Validator;

class SuperAdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addAdmin(){

    	return view('/admin/add_admin');
    }

    public function submitAdmin(Request $request){

    	$validator = Validator::make($request->all(), [
            
        'name' => 'required|regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/',
        'phone_no' => 'required|max:10|min:10',
        'email' => 'required',
        'password' => 'required|max:8|min:6'  
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $name = $request->input('name');
        $phone_no = $request->input('phone_no');
        $email = $request->input('email');
        $password = $request->input('password');
        $user_type ="admin";

        $submit_admin = new AllUsers();
        $submit_admin->name = $name;
        $submit_admin->phone_no = $phone_no;
        $submit_admin->email = $email;
        $submit_admin->password = Hash::make($password);
        $submit_admin->user_type = $user_type;
        $submit_admin->admin_password = $password;
        $submit_admin->save();

        return redirect('/admin_list')->with('success','Successfully added.');
    }
   
    public function admin_list(){

   	    $admin_detail = AllUsers::where('user_type','admin')->where('status','1')->get();
   	    return view ('/admin/admin_list' , compact('admin_detail'));
    }

    public function edit_admin_detail( $id ){

       	$edit_admin_detail = AllUsers::where('id',$id)->where('user_type','admin')->first();
       	return view('/admin/edit_admin_detail', compact('edit_admin_detail'));
    }

    public function update_admin_detail(Request $request , $id){

   		$validator = Validator::make($request->all(), [
            
        'name' => 'required|regex:/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/',
        'phone_no' => 'required|min:10|max:10',
        'email' => 'required',
        'admin_password' => 'required|max:8|min:6'  
        ]);

        if ($validator->fails()) 
        {
            return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $name = $request->input('name');
        $phone_no = $request->input('phone_no');
        $email = $request->input('email');
        $admin_password = $request->input('admin_password');
        $user_type ="admin";

        $submit_admin = AllUsers::find($id);
        $submit_admin->name = $name;
        $submit_admin->phone_no = $phone_no;
        $submit_admin->email = $email;
        $submit_admin->admin_password = $admin_password;
        $submit_admin->user_type = $user_type;
        $submit_admin->save();

        return redirect('/admin_list')->with('success','Successfully updated.');
    }

    public function delete_admin_detail($id)
    {
        $deletestatement = AllUsers::find($id);

        $deletestatement->status = "0";
        $deletestatement->save();
        //pa($deletepost); die();
        return redirect()->back()->with('success', 'Statement Deleted Successfully');
    }

    //shows all admins detail 

    public function category_added_by_admin(){

        $category_detail = AdminCategory::all();
        return view('/admin/admin_category_detail',compact('category_detail'));
    } 
    
    public function admin_add_subject_list(){

        $subject_list = DB::table('add_subject')
            ->join('add_category', 'add_subject.category_id', '=', 'add_category.category_id')
             ->get();
        return view('/admin/admin_add_subject_list',compact('subject_list'));
    }

    //*------------Questions Details------------*//

    public function mcqs_list(){

        $mcqs_list =   AddQuestion::with('subject_details','category_details')->where('question_type','mcqs')->where('del_status','active')->paginate(10); 
        return view('/admin/mcqs_list',compact('mcqs_list'));
    }

    public function multiple_response_qs(){

        $mrqs_list = AddQuestion::with('subject_details','category_details')->where('question_type','mrqs')->where('del_status','active')->paginate(10);
        return view('/admin/mrqs_list',compact('mrqs_list'));
    }

    public function true_false_qs(){
        $true_false_list = AddQuestion::with('subject_details','category_details')->where('question_type','true_false')->where('del_status','active')->paginate(10);
        return view('/admin/true_false_qs',compact('true_false_list'));
    }

    public function short_qs(){
        $short_qs_list = AddQuestion::with('subject_details','category_details')->where('question_type','short_answer')->where('del_status','active')->paginate(10);
        return view('/admin/short_qs_list',compact('short_qs_list'));
    }

    public function numeric_qs_list(){

        $numeric_qs_list = AddQuestion::with('subject_details','category_details')->where('question_type','numeric_question')->where('del_status','active')->paginate(10);
        return view('/admin/numeric_questions_list',compact('numeric_qs_list'));
    }

    public function essay_questions_list(){

        $essay_qs_list = AddQuestion::with('subject_details','category_details')->where('question_type','numeric_question')->where('del_status','active')->paginate(10);
        return view('/admin/essay_qs_list',compact('essay_qs_list'));
    }

    public function demographic_qs_list(){

        $demographic_qs_list = AddQuestion::with('subject_details','category_details')->where('question_type','demographic_questions')->where('del_status','active')->paginate(10);
        return view('/admin/demographic_qs_list',compact('demographic_qs_list'));
    }
//end class   
}