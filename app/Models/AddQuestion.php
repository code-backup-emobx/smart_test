<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AddQuestion extends Model
{
    protected $primaryKey = 'question_id';
    protected $table = 'add_questions';
   
   	public function subject_details(){
        
        return $this->belongsTo('App\Models\SubjectDetail', 'subject_name');
    }

    public function category_details(){
        
        return $this->belongsTo('App\Models\AdminCategory', 'category_id');
    }    
    
    public function institute_details(){
        
        return $this->belongsTo('App\Models\Institutes', 'institute_name');
    }
}