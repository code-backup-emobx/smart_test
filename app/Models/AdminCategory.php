<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AdminCategory extends Model
{
    protected $primaryKey = 'category_id';
    protected $table = 'add_category';
  
}