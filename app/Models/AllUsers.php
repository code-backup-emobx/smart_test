<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AllUsers extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'users';
}