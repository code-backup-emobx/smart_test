<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class InstituteStudents extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'institute_add_students';   


    public function institute_details(){
        
        return $this->belongsTo('App\Models\Institutes', 'institute_name');
    } 
    public function student_details(){
        
        return $this->belongsTo('App\Models\AllUsers', 'student_name')->where('user_type','institute_student');
    } 
//end class 
}