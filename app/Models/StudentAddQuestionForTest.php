<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class StudentAddQuestionForTest extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'add_questions_for_test';
}