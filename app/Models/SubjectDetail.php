<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SubjectDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'add_subject';
    
     public function category_details(){
        
        return $this->belongsTo('App\Models\AdminCategory', 'category_id');
    } 
}