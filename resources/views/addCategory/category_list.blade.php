@extends('layouts.admin_master')
@section('content')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-user"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Category List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
                                        <a href="/add_category" class="btn btn-info"><i class="flaticon2-plus-1"></i> Add Category</a>
                                    </div>
								</div>
								<div class="kt-portlet__body">
                                      <div class="table-overflow">
									<!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th> Category Id </th>
												<th> Category Name </th>
												<th> Category Status </th>
												<th> Action </th>
											</tr>
										</thead>
										<tbody>
											
											@foreach($category_list as $category_listing)
											<tr>
												<td> {{ $category_listing->category_id }} </td>
												<td> {{ $category_listing->category_name }} </td>
												<td> {{ $category_listing->status }} </td>
                                                <td class="text-left">
                                                    <a href=" {{ url('/edit_category/'.$category_listing->category_id)}} " class="btn btn-brand btn-sm fa fa-edit"> Edit </a>

                                                    <a href=" {{ url('/delete_category_detail/'.$category_listing->category_id) }} " class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')"> <i class="fa fa-trash"></i> Delete </a>
                                                </td>
											</tr>
											@endforeach
											
										</tbody>
									</table>
									<!--end: Datatable -->
								    </div>
								   
                                </div>
                            </div>
						</div>
						<!-- end:: Content -->
					<!-- begin:: Footer -->
				 


@endsection