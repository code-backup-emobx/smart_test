@extends('layouts.admin_master')
@section('content')
					<!-- end:: Header -->
				<div class="kt-grid kt-grid--hor kt-grid--root">
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-user"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Update Category
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar"></div>
								</div>
								
                                <!--begin::Form-->
                                <form class="kt-form" method="post" action="{{ url('/edit_category/'.$edit_category_detail->category_id) }}">
                                    {{ csrf_field() }}
                                    <div class="kt-portlet__body">
                                        <div class="kt-section">
                                            <div class="kt-section__body">
                                                
                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"> Category Name: </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Enter add category" name="category_name" value=" {{ $edit_category_detail->category_name }} "  id="category_name">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label">Category Status: </label>
                                                    <div class="col-lg-6">

                                                        <select class="form-control" name="status" id="status">
                                                            <option value="active" {{ $edit_category_detail->status == 'active' ? 'selected="selected"' : '' }}> Active </option>
                                                            <option value="inactive" {{ $edit_category_detail->status == 'inactive' ? 'selected="selected"' : '' }}> Inactive </option>

                                                        </select>

                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-6">
                                                    <input type="submit" class="btn btn-success" value="Submit">
                                                    <a onclick="history.go(-1)" class="btn btn-secondary">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!--end::Form-->
									
							</div>
						</div>
						<!-- end:: Content -->
                </div>
				
                @endsection    
					<!-- begin:: Footer -->
