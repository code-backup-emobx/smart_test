@extends('layouts.admin_master')
@section('content')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-user"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Subject List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
                                        <a href="/add_subject" class="btn btn-info"><i class="flaticon2-plus-1"></i> Add Subject</a>
                                    </div>
								</div>
								<div class="kt-portlet__body">
                                      <div class="table-overflow">
									<!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th> Subject Id </th>
												<th> Subject Name </th>
												<th> Category Name </th>
												<th> Duration </th>
												<th> Action </th>
											</tr>
										</thead>
										<tbody>
											
											@foreach($subject_list as $listing)
											<tr>
												<td> {{ $listing->id }} </td>
												<td> {{ $listing->subject_name }} </td>
												<td> {{ $listing['category_details']->category_name }} </td>
												<td> {{ $listing->duration }} </td>
                                                <td class="text-left">
                                                    <a href=" {{ url('/edit_subject/'.$listing->id)}} " class="btn btn-brand btn-sm fa fa-edit"> Edit </a>

                                                    <a href=" {{ url('/delete_subject/'.$listing->id) }} " class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')"> <i class="fa fa-trash"></i> Delete </a>
                                                </td>
											</tr>
											@endforeach
											
											
										</tbody>
									</table>
									<!--end: Datatable -->
								    </div>
								   
                                </div>
                            </div>
						</div>
						<!-- end:: Content -->
					<!-- begin:: Footer -->
				 


@endsection