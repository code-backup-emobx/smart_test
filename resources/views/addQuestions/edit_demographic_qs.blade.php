@extends('layouts.admin_master')
@section('content')
	<!-- end:: Header -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-question"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Update Demographic Questions
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar"></div>
				</div>
				
                <!--begin::Form-->
                <form class="kt-form" method="post" action=" {{ url('/edit_demographic_qs/'.$update_demographic_question->question_id) }} ">
                    {{ csrf_field() }}
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
								<div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Subject Name: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="subject_name" id="subject_name" >
                                                <option> --select-- </option>
                                                @foreach($subject_listing as $listing)
                                                <option value="{{$listing->id}}" {{ $update_demographic_question->subject_name == $listing->id ? 'selected="selected"' : '' }}> {{ $listing->subject_name }} </option>
                                                @endforeach
										   </select>
                                        </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Category Name: </label>
                                        <div class="col-lg-6">
                                            <select class="form-control" name="category_name" id="category_name" >
                                                <option> --select-- </option>
                                                @foreach($catg_listing as $listing)
                                                <option value="{{$listing->category_id}}" {{ $update_demographic_question->category_id == $listing->category_id ? 'selected="selected"' : '' }}> {{ $listing->category_name }} </option>
                                                @endforeach
										   </select>
                                        </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-lg-8 col-form-label"> Question: </label>
                                    <div class="col-lg-9">
                                        <textarea id="form7" class="md-textarea form-control" rows="3" name="question_text" >{{ $update_demographic_question->question_text }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <h5 class="col-lg-8">Add Options</h5>
                                    <label class="col-lg-8 col-form-label"> Option 1: </label>
                                    <div class="col-lg-9">
                                       <input type="text" class="form-control" name="option_one" placeholder="Enter option one" value=" {{ $update_demographic_question->option_one }} " >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-8 col-form-label"> Option 2: </label>
                                    <div class="col-lg-9">
                                       <input type="text" class="form-control" name="option_two" placeholder="Enter option two" value=" {{ $update_demographic_question->option_two }} ">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-8 col-form-label"> Option 3: </label>
                                    <div class="col-lg-9">
                                       <input type="text" class="form-control" name="option_three" placeholder="Enter option three" value=" {{ $update_demographic_question->option_three }} ">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-8 col-form-label"> Option 4: </label>
                                    <div class="col-lg-9">
                                       <input type="text" class="form-control" name="option_four" placeholder="Enter option four" value=" {{ $update_demographic_question->option_four }} ">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-8 col-form-label"> Correct Answer: </label>
                                    <div class="col-lg-9">
                                       <input type="text" class="form-control" name="correct_answer" placeholder="Enter correct answer" value="{{ $update_demographic_question->correct_answer }}">
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <input type="submit" class="btn btn-success" value="Submit">
                                    <a onclick="history.go(-1)" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
					
			</div>
		</div>
		<!-- end:: Content -->
</div>

@endsection    
	<!-- begin:: Footer -->
