@extends('layouts.admin_master')
@section('content')
	<!-- end:: Header -->
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-question"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Essay Question List
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
                        <a href="/add_essay_question" class="btn btn-info"><i class="flaticon2-plus-1"></i> Add Essay Question</a>
                    </div>
				</div>
				<div class="kt-portlet__body">
                      <div class="table-overflow">
					<!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
						<thead>
							<tr>
								<th> Question Id </th>
								<th> Question Text </th>
								<th> Category Name </th>
								<th> Subject Name </th>
								<th> Created Date </th>
								<th> Action </th>
							</tr>
						</thead>
						<tbody>
							@if($essay_questions_list->total()==0)
                                <tr><td colspan="12"><center><h5>" No record Found "</h5></center></td></tr>
                            @endif
							
							@foreach($essay_questions_list as $essay_qs_listing)
							<tr>
								<td> {{ $essay_qs_listing->question_id }} </td>
								<td> {{ $essay_qs_listing->question_text }} </td>
								<td> {{ $essay_qs_listing['category_details']->category_name }} </td>
								<td> {{ $essay_qs_listing['subject_details']->subject_name }} </td>
								<td> {{ $essay_qs_listing->created_at }} </td>
								
                                <td class="text-left">
                                    <a href="{{ url('/edit_essay_qs/'.$essay_qs_listing->question_id) }}" class="btn btn-brand btn-sm fa fa-edit"> Edit </a>

                                    <a href=" {{ url('/delete_essay_question/'.$essay_qs_listing->question_id) }} " class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')"> <i class="fa fa-trash"></i> Delete </a>
                                </td>
							</tr>
							@endforeach
							
							
						</tbody>
					</table>
					<!--end: Datatable -->
				    </div>
				   
                </div>
				{{ $essay_questions_list->links() }} 

            </div>
		</div>
		<!-- end:: Content -->
	<!-- begin:: Footer -->
				 


@endsection