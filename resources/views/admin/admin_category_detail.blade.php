@extends('layouts.master')
@section('content')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-user"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Admin Add CategoryList
										</h3>
									</div>
									<!-- <div class="kt-portlet__head-toolbar">
                                        <a href="/add_admin" class="btn btn-info"><i class="flaticon2-plus-1"></i> Add New Admin</a>
                                    </div> -->
								</div>
								<div class="kt-portlet__body">
                                      <div class="table-overflow">
									<!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th> Category Id </th>
												<th> Category Name </th>
												<th> Admin Name </th>
												<th> Status </th>
												<th> Created At</th>
												
											</tr>
										</thead>
										<tbody>
											
											@foreach($category_detail as $cat_detail_listing)
											<tr>
												<td> {{ $cat_detail_listing->category_id }} </td>
												<td> {{ $cat_detail_listing->category_name }} </td>
												<td> {{ $cat_detail_listing->login_admin_name }} </td>
												<td> {{ $cat_detail_listing->status }} </td>
												<td> {{ $cat_detail_listing->created_at }} </td>
											</tr>
											@endforeach
											
										</tbody>
									</table>
									<!--end: Datatable -->
								    </div>
								   
                                </div>
                            </div>
						</div>
						<!-- end:: Content -->
					<!-- begin:: Footer -->
				 


@endsection