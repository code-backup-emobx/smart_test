@extends('layouts.master')
@section('content')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand la la-user"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Admin List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
                                        <a href="/add_admin" class="btn btn-info"><i class="flaticon2-plus-1"></i> Add New Admin</a>
                                    </div>
								</div>
								<div class="kt-portlet__body">
                                      <div class="table-overflow">
									<!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th> Admin Id </th>
												<th> Admin Name </th>
												<th> Admin Phone Number </th>
												<th> Admin Email </th>
												<th> Admin Password </th>
												<th> Action </th>
											</tr>
										</thead>
										<tbody>
											@foreach($admin_detail as $detail_listing)
											<tr>
												<td> {{ $detail_listing->id }} </td>
												<td> {{ $detail_listing->name }} </td>
												<td> {{ $detail_listing->phone_no }} </td>
												<td>  {{ $detail_listing->email }}</td>
												<td>  {{ $detail_listing->admin_password }}</td>

                                                <td class="text-left">
                                                    <a href=" {{ url('/edit_admin_detail/'.$detail_listing->id)}} " class="btn btn-brand btn-sm fa fa-edit"> Edit </a>
                                                    
                                                    <a href=" {{ url('/delete_admin_detail/'.$detail_listing->id) }} " class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')"> <i class="fa fa-trash"></i> Delete </a>
                                                </td>
											</tr>
											@endforeach
											
										</tbody>
									</table>
									<!--end: Datatable -->
								    </div>
								   
                                </div>
                            </div>
						</div>
						<!-- end:: Content -->
					<!-- begin:: Footer -->
				 


@endsection