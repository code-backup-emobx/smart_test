@extends('layouts.master')
@section('content')
					<!-- end:: Header -->
				<div class="kt-grid kt-grid--hor kt-grid--root">
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-animals-2"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Update Admin Detail
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar"></div>
								</div>
								
                                <!--begin::Form-->
                                <form class="kt-form" method="post" action="{{ url('/edit_admin_detail/'.$edit_admin_detail->id) }}">
                                    {{ csrf_field() }}
                                    <div class="kt-portlet__body">
                                        <div class="kt-section">
                                            <div class="kt-section__body">
                                               <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"> Admin Name: </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Enter admin name" name="name" value="{{ $edit_admin_detail->name }}"  id="admin_name">
                                                    </div>
                                                </div>

                                                 <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"> Admin Phone Number: </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Enter admin phone number" name="phone_no" value=" {{ $edit_admin_detail->phone_no }} "  id="phone_no">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"> Admin Email: </label>
                                                    <div class="col-lg-6">
                                                        <input type="email" class="form-control" placeholder="Enter admin email" name="email" value="{{ $edit_admin_detail->email }}"  id="email">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"> Admin Password: </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" placeholder="Enter admin password" name="admin_password" value="{{ $edit_admin_detail->admin_password }}"  id="admin_password">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-6">
                                                    <input type="submit" class="btn btn-success" value="Submit">
                                                    <a onclick="history.go(-1)" class="btn btn-secondary">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!--end::Form-->
									
							</div>
						</div>
						<!-- end:: Content -->
                </div>
				
                @endsection    
					<!-- begin:: Footer -->
