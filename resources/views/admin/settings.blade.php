@include('layouts.header')
<!-- @section('content') -->
		<!-- end:: Header Mobile -->
		@include('layouts.sidebar')
				<!-- end:: Aside -->
            @include('layouts.topbar')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-settings-1"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Settings
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar"></div>
								</div>
								
                                <!--begin::Form-->
                                <form class="kt-form">
                                    <div class="kt-portlet__body">
                                        <div class="kt-section">
                                            <div class="kt-section__body">
                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"> Change Theme Color: </label>
                                                    <div class="col-lg-6">
                                                        <input type="color" class="theme-changes" name="blastCustomColor" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label"> Change Text Color: </label>
                                                    <div class="col-lg-6">
                                                        <input type="color" class="theme-changes" name="blastCustomColortext" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!--end::Form-->
							</div>
						</div>
						<!-- end:: Content -->
                        
					</div>

					<!-- begin:: Footer -->
					@include('layouts.footer')	