@extends('layouts.master')
@section('content')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand fa fa-question"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											MCQ'S List
										</h3>
									</div>
									<!-- <div class="kt-portlet__head-toolbar">
                                        <a href="/add_admin" class="btn btn-info"><i class="flaticon2-plus-1"></i> Add New Admin</a>
                                    </div> -->
								</div>
								<div class="kt-portlet__body">
                                      <div class="table-overflow">
									<!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th> Question Id </th>
												<th> Question  </th>
												<th> Subject Name </th>
												<th> Category Name </th>
												<th> Duration </th>
												<th> Admin Name </th>
												<th> Status </th>
												<th> Created At </th>

											</tr>
										</thead>
										<tbody>
											@if($short_qs_list->total()==0)
				                                <tr><td colspan="12"><center><h5>" No record Found "</h5></center></td></tr>
				                            @endif
											@foreach( $short_qs_list as $short_qs_listing)
											<tr>
												<td> {{ $short_qs_listing->question_id }} </td>
												<td> {{ $short_qs_listing->question_text }} </td>
												<td> {{ $short_qs_listing['subject_details']->subject_name }} </td>
												<td> {{ $short_qs_listing['category_details']->category_name }} </td>
												<td> {{ $short_qs_listing['subject_details']->duration }} </td>
												<td> {{ $short_qs_listing->login_admin_name }} </td>
												<td>  {{ $short_qs_listing->del_status }} </td>
												<td>  {{ $short_qs_listing->created_at }} </td>

											</tr>
											@endforeach
											
										</tbody>
									</table>
									<!--end: Datatable -->
								    </div>
									{{ $short_qs_list->links() }} 
								   
                                </div>
                            </div>
						</div>
						<!-- end:: Content -->
					<!-- begin:: Footer -->
				 


@endsection