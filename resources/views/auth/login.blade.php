@extends('layouts.login_tmp')
	<!-- begin::Body -->
@section('content')
	<div class="kt-login__signin">
		<form class="kt-form" action="{{ url('/login') }}" method="post">
			{{ csrf_field() }}
			<div class="input-group">
				<input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
			</div>
			<div class="input-group">
				<input class="form-control" type="password" placeholder="Password" name="password">
			</div>
			<div class="row kt-login__extra">
				<div class="col">
					<label class="kt-checkbox">
						<input type="checkbox" name="remember"> Remember me
						<span></span>
					</label>
				</div>
			</div>
			<div class="kt-login__actions">
				<button type="submit" name="login_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button>
			</div>
			
		</form>
		
	</div>
@endsection
