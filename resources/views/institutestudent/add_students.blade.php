@extends('layouts.admin_master')
@section('content')
    <!-- end:: Header -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand fa fa-university"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Add Students In Institues
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar"></div>
                </div>
                
                <!--begin::Form-->
                <form class="kt-form" method="post" action="/add_students">
                    {{ csrf_field() }}
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Institute Name: </label>
                                    <div class="col-lg-6">
                                        <select name="institute_name" class="form-control">
                                        	<option value="">--select--</option>
                                        	@foreach($institute_list as $institute_listing)
                                        		<option value="{{ $institute_listing->id }}">{{ $institute_listing->institute_name }}</option>
                                        	@endforeach	
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Student Name: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="student_name" placeholder="Enter student name" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Student Password: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="student_password" placeholder="Enter student password" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Student Email: </label>
                                    <div class="col-lg-6">
                                        <input type="email" class="form-control" name="student_email" placeholder="Enter student email" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Student Phone Number: </label>
                                    <div class="col-lg-6">
                                        <input type="number" class="form-control" name="student_phone_number" placeholder="Enter student phone number" value="">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <input type="submit" class="btn btn-success" value="Submit">
                                    <a onclick="history.go(-1)" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
                    
            </div>
        </div>
        <!-- end:: Content -->
</div>

@endsection    
    <!-- begin:: Footer -->
