@extends('layouts.admin_master')
@section('content')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand fa fa-university"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Institute List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
                                        <a href="/add_institutes" class="btn btn-info"><i class="flaticon2-plus-1"></i> Add institute</a>
                                    </div>
								</div>
								<div class="kt-portlet__body">
                                      <div class="table-overflow">
									<!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th> Institute Id </th>
												<th> Institute Name </th>
												<th> Institute Status </th>
												<th> Action </th>
											</tr>
										</thead>
										<tbody>
											@foreach($institutes_list as $institute_listing)
												<tr>
													<td> {{ $institute_listing->id }} </td>
													<td> {{ $institute_listing->institute_name }} </td>
													<td> {{ $institute_listing->status }} </td>
	                                                <td class="text-left">
	                                                    <a href=" {{ url('/edit_institute/'.$institute_listing->id) }} " class="btn btn-brand btn-sm fa fa-edit"> Edit </a>

	                                                    <a href=" {{ url('/delete_institute/'.$institute_listing->id) }} " class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')"> <i class="fa fa-trash"></i> Delete </a>
	                                                </td>
												</tr>
											@endforeach
											
										</tbody>
									</table>
									<!--end: Datatable -->
								    </div>
								   
                                </div>
                            </div>
						</div>
						<!-- end:: Content -->
					<!-- begin:: Footer -->
				 


@endsection