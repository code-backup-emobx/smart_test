@extends('layouts.admin_master')
@section('content')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand fa fa-user"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Students List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
                                        <a href="/add_students" class="btn btn-info"><i class="flaticon2-plus-1"></i> Add Student</a>
                                    </div>
								</div>
								<div class="kt-portlet__body">
                                      <div class="table-overflow">
									<!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th> Student Id </th>
												<th> Institute Name </th>
												<th> Student Name </th>
												<th> Student Status </th>
											</tr>
										</thead>
										<tbody>
											@if($get_students_list->total()==0)
                                                <tr><td colspan="12"><center><h5>" No record Found "</h5></center></td></tr>
                                            @endif
											@foreach($get_students_list as $student_listing)
												<tr>
													<td> {{ $student_listing->id }} </td>
													<td> {{ $student_listing['institute_details']->institute_name }} </td>
													<td> {{ $student_listing['student_details']->name }} </td>
													<td> {{ $student_listing->student_status }} </td>
	                                                <!-- <td class="text-left">
	                                                    <a href=" {{ url('/edit_institute/'.$student_listing->id) }} " class="btn btn-brand btn-sm fa fa-edit"> Edit </a>

	                                                    <a href=" {{ url('/delete_institute/'.$student_listing->id) }} " class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this?')"> <i class="fa fa-trash"></i> Delete </a>
	                                                </td> -->
												</tr>
											@endforeach
											
										</tbody>
									</table>
									<!--end: Datatable -->
								    </div>
   										 {{ $get_students_list->links() }} 
								   
                                </div>
                            </div>
						</div>
						<!-- end:: Content -->
					<!-- begin:: Footer -->
				 


@endsection