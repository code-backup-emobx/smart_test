@include('layouts.header')

   
            <!-- BEGIN HEADER -->
            @include('layouts.admin_topbar')
            <!-- END HEADER -->
             <!-- BEGIN HEADER & CONTENT DIVIDER -->
           
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            
                <!-- BEGIN SIDEBAR -->
                @include('layouts.admin_sidebar')
                <!-- END SIDEBAR -->
                
                    <!-- BEGIN CONTENT -->
                
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">    
                    @include('layouts.notification')
                     @yield('content')
                   
               
                @include('layouts.footer')