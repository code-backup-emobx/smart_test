@include('layouts.header')
   
            <!-- BEGIN HEADER -->
            @include('layouts.student_topbar')
            <!-- END HEADER -->
             <!-- BEGIN HEADER & CONTENT DIVIDER -->
           
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            
                <!-- BEGIN SIDEBAR -->
                @include('layouts.student_sidebar')
                <!-- END SIDEBAR -->
                
                    <!-- BEGIN CONTENT -->
                
                    
                    <!-- BEGIN CONTENT BODY -->
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">    
                    @include('layouts.notification')
                     @yield('content')
                   
               
                @include('layouts.footer')