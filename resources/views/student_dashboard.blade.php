@extends('layouts.student_master')
@section('content')
					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

						<!-- begin:: Content Head -->
						<!--<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-subheader__main">
								<h3 class="kt-subheader__title">Dashboard</h3>
							</div>
						</div>-->
						<!-- end:: Content Head -->

						<!-- begin:: Content -->
						<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

							<!--Begin::Dashboard-->
                            
                            <!--Begin::Section-->
                            <div class="row">
                                <div class="col-xl-3 col-md-3">
									<!--begin:: Widgets/No. of Users-->
									<div class="kt-portlet kt-portlet--height-fluid prevu_kit">
										<div class="kt-portlet__body kt-portlet__body--fluid">
											<div class="kt-widget20">
												<div class="kt-widget20__content text-center">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <span class="kt-widget20__number kt-font-brand counter">11 </span>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <span class="kt-widget20__number kt-font-brand"> + </span>
                                                        </li>
                                                    </ul>
													<span class="kt-widget20__desc">Loreum</span>
												</div>
											</div>
										</div>
									</div>
									<!--end:: Widgets/No. of Users-->
								</div>
                                
                                <div class="col-xl-3 col-md-3">
									<!--begin:: Widgets/No. of Pets-->
									<div class="kt-portlet kt-portlet--height-fluid prevu_kit">
										<div class="kt-portlet__body kt-portlet__body--fluid">
											<div class="kt-widget20">
												<div class="kt-widget20__content text-center">
													<ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <span class="kt-widget20__number kt-font-brand counter">12 </span>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <span class="kt-widget20__number kt-font-brand"> + </span>
                                                        </li>
                                                    </ul>
													<span class="kt-widget20__desc">Loreum</span>
												</div>
											</div>
										</div>
									</div>
									<!--end:: Widgets/No. of Pets-->
								</div>
                                
                                <div class="col-xl-3 col-md-3">
									<!--begin:: Widgets/No. of Posts-->
									<div class="kt-portlet kt-portlet--height-fluid prevu_kit">
										<div class="kt-portlet__body kt-portlet__body--fluid">
											<div class="kt-widget20">
												<div class="kt-widget20__content text-center">
													<ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <span class="kt-widget20__number kt-font-brand counter">23 </span>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <span class="kt-widget20__number kt-font-brand"> + </span>
                                                        </li>
                                                    </ul>
													<span class="kt-widget20__desc">Loreum</span>
												</div>
											</div>
										</div>
									</div>
									<!--end:: Widgets/No. of Posts-->
								</div>
                                
                                <div class="col-xl-3 col-md-3">
									<!--begin:: Widgets/No. of Categories-->
									<div class="kt-portlet kt-portlet--height-fluid prevu_kit">
										<div class="kt-portlet__body kt-portlet__body--fluid">
											<div class="kt-widget20">
												<div class="kt-widget20__content text-center">
													<ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <span class="kt-widget20__number kt-font-brand counter">52</span>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <span class="kt-widget20__number kt-font-brand"> + </span>
                                                        </li>
                                                    </ul>
													<span class="kt-widget20__desc">Loreum</span>
												</div>
											</div>
										</div>
									</div>
									<!--end:: Widgets/No. of Categories-->
								</div>
                            </div>
                            <!--End:: Section-->
                            
                            <!--Begin::Section-->
                            <div class="row">
								<div class="col-lg-12">

									<!--begin::Portlet-->
									<div class="kt-portlet kt-portlet--tab">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon kt-hidden">
													<i class="la la-gear"></i>
												</span>
												<h3 class="kt-portlet__head-title">
													loreum ipsum dolor sit amet
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">
											
										<figure class="highcharts-figure">
										    <div id="tendays"></div>
										  
										</figure>
										</div>
									</div>

									<!--end::Portlet-->
								</div>
							</div>
                            <!--End::Section-->
                            
                            <!--Begin::Section-->
                            <!-- <div class="row">
								<div class="col-lg-12">

									begin::Portlet-->
									<!-- <div class="kt-portlet kt-portlet--tab">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon kt-hidden">
													<i class="la la-gear"></i>
												</span>
												<h3 class="kt-portlet__head-title">
													New Pets Added Last Month
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">
											<div id="kt_flotcharts_6" style="height: 300px;"></div>
										</div>
									</div> -->

									<!--end::Portlet-->
							<!-- 	</div>
							</div> -->
                            <!--End::Section-->

                            <!--Begin::Section-->
                            <!-- <div class="row">
								<div class="col-lg-12">
									begin::Portlet-->
									<!-- <div class="kt-portlet kt-portlet--tab">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon kt-hidden">
													<i class="la la-gear"></i>
												</span>
												<h3 class="kt-portlet__head-title">
													Pets lost pet found pet adopt
												</h3>
											</div>
										</div> -->
										<!-- <div class="kt-portlet__body">
											<div id="kt_flotcharts_2" style="height: 300px;"></div>
										</div>
									</div> -->

									<!--end::Portlet-->
								<!-- </div>
							</div>  -->
                            <!--End::Section-->

							<!--End::Dashboard-->
                            
						</div>
						<!-- end:: Content -->
					</div>

					<!-- begin:: Footer -->
				@endsection 	
