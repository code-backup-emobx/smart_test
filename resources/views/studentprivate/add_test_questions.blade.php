@extends('layouts.student_master')
@section('content')
<?php error_reporting(0); ?>
                    <!-- end:: Header -->
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

                        <!-- begin:: Content -->
                        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--lg">
                                    <div class="kt-portlet__head-label">
                                        <span class="kt-portlet__head-icon">
                                            <i class="kt-font-brand fa fa-question"></i>
                                        </span>
                                        <h3 class="kt-portlet__head-title">
                                            Add Questions for test p
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        
                                        <form class="form-horizontal" method="GET" action="{{ '/add_test_questions' }}">
                                            <select name="status" id="status" class="col-lg-6 form-control">
                                               
                                                <option >--Choose question type</option>
                                                <option value="mcqs" @if($status == 'mcqs') selected="mcqs" @endif>MCQS</option>

                                                <option value="mrqs" @if($status == 'mrqs') selected="mrqs" @endif>Multiple Responce Questions</option>

                                                <option value="true_false" @if($status == 'true_false') selected="true_false" @endif>True False</option>

                                            <option value="short_answer" @if($status == 'short_answer') selected="short_answer" @endif>Short Qnswer</option>

                                                <option value="numeric_question" @if($status == 'numeric_question') selected="numeric_question" @endif>Numeric Questions</option>

                                                <option value="essay_question" @if($status == 'essay_question') selected="essay_question" @endif>Essay Questions</option>
                                                
                                                <option value="demographic_questions" @if($status == 'demographic_questions') selected="demographic_questions" @endif>Demographic Questions</option>
                                            </select>
                                            <div class="col-xs-1"></div>
                                            <input type="submit" class="btn btn-info" value="Submit">
                                        </form>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                      <div class="table-overflow">
                                    <!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                        <thead>
                                            <tr>
                                                <th> Question Id </th>
                                                <th> Institute Name </th>
                                                <th> Category Name </th>
                                                <th> Subject Name </th>
                                                <th> Question Text </th>
                                                <th> Option One </th>
                                                <th> Option Two </th>
                                                <th> Option Three </th>
                                                <th> Option Four </th>
                                                <th> Correct Answer </th>
                                                <th> Duration </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach( $data as $question_list)
                                            <tr>
                                                <td> {{ $question_list->question_id }} </td>
                                                <td> {{ $question_list['institute_details']->institute_name }} </td>
                                                <td> {{ $question_list['category_details']->category_name }} </td>
                                                <td> {{ $question_list['subject_details']->subject_name }} </td>
                                                <td> {{ $question_list->question_text }} </td>
                                                <td> {{ $question_list->option_one }} </td>
                                                <td> {{ $question_list->option_two }} </td>
                                                <td> {{ $question_list->option_three }} </td>
                                                <td> {{ $question_list->option_four }} </td>
                                                <td> {{ $question_list->correct_answer }} </td>
                                                <td> {{ $question_list->duration }} </td>
                                                
                                                <td class="text-left">
                                                    
                                                    <a href=" {{ url('/selected_questions_add/'.$question_list->question_id) }} " class="btn btn-brand btn-sm"> Add Question </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            
                                        </tbody>
                                    </table>
                                    <!--end: Datatable -->
                                    </div>
                                </div>
                                   
                            </div>
                        </div>
                        <!-- end:: Content -->
                    <!-- begin:: Footer -->
@endsection