@extends('layouts.student_master')
@section('content')
	<!-- end:: Header -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
		<!-- begin:: Content -->
		<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
			<div class="kt-portlet kt-portlet--mobile">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand fa fa-question"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Add Questions
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar"></div>
				</div>
				
                <!--begin::Form-->
                <form class="kt-form" method="post" action="{{ url('/selected_questions_add/'.$question_of_detail->question_id) }}">
                    {{ csrf_field() }}
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Institute Name: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="institute_name" value="{{ $question_of_detail['institute_details']->institute_name }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Subject Name: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="subject_name" value="{{ $question_of_detail['subject_details']->subject_name }}">
                                    </div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Category Name: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="category_name" value="{{ $question_of_detail['category_details']->category_name }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Question: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="question_name" value="{{ $question_of_detail->question_text }}" >
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                	<div class="col-lg-12">
                                		<div class="col-lg-6">
		                                    <label class="col-lg-2 col-form-label"> Option 1: </label>
		                                    <div class="col-lg-5">
		                                        <input type="text" class="form-control" name="option_one" value="{{ $question_of_detail->option_one }}" >
		                                    </div>
		                                </div>

		                                <div class="col-lg-6">
		                                    <label class="col-lg-2 col-form-label"> Option 2: </label>
		                                    <div class="col-lg-4">
		                                        <input type="text" class="form-control" name="option_two" value="{{ $question_of_detail->option_two }}">
		                                    </div>
		                                </div>    
                                	</div>
                                </div>

                                <div class="form-group row">
                                	<div class="col-lg-12">
                                		<div class="col-lg-6">
		                                    <label class="col-lg-2 col-form-label"> Option 3: </label>
		                                    <div class="col-lg-5">
		                                        <input type="text" class="form-control" name="option_three" value="{{ $question_of_detail->option_three }}">
		                                    </div>
		                                </div>

		                                <div class="col-lg-6">
		                                    <label class="col-lg-2 col-form-label"> Option 4: </label>
		                                    <div class="col-lg-4">
		                                        <input type="text" class="form-control" name="option_four" value="{{ $question_of_detail->option_four }}">
		                                    </div>
		                                </div>    
                                	</div>
                                </div>

                                 <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Correct Answer: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="correct_answer" value="{{ $question_of_detail->correct_answer }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Duration: </label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="duration" value="{{ $question_of_detail->duration }}" >
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <input type="submit" class="btn btn-success" value="Submit">
                                    <a onclick="history.go(-1)" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
					
			</div>
		</div>
		<!-- end:: Content -->
</div>

@endsection    
	<!-- begin:: Footer -->
