@extends('layouts.student_master')
@section('content')
    <!-- end:: Header -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
        <!-- begin:: Content -->
        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="kt-font-brand fa fa-university"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Choose Questions for Test
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar"></div>
                </div>
                
                <!--begin::Form-->
                <form class="kt-form" method="post" action="/create_test">
                    {{ csrf_field() }}
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__body">
                                
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"> Choose Test Type: </label>
                                    <div class="col-lg-6">
                                        <select class="form-control" name="choose_test_type">
                                        	<option value="">--Select--</option>
                                        	<option value="public">Public</option>
                                        	<option value="private">Private</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <input type="submit" class="btn btn-success" value="Submit">
                                    <a onclick="history.go(-1)" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
                    
            </div>
        </div>
        <!-- end:: Content -->
</div>

@endsection    
    <!-- begin:: Footer -->
