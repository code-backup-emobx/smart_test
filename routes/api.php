<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'ApiController@login');
Route::post('add_multiple_admin','ApiController@add_multiple_admin');
Route::get('multiple_admins_list','ApiController@multiple_admins_list');

/* admin add category of questions*/
Route::post('add_questions_category','ApiController@add_questions_category');

/* admin add subject of questions*/ 
Route::post('add_question_subject','ApiController@add_question_subject');

/* admin add multiple choice type of questions*/
Route::post('add_mcqs_question','ApiController@add_mcqs_question');

/* add multiple response questions*/ 
Route::post('add_multiple_response_questions','ApiController@add_multiple_response_questions');

/* add true false type question*/ 
Route::post('add_true_false_questions','ApiController@add_true_false_questions');

/* add short questions*/
Route::post('add_short_question','ApiController@add_short_question');

/* add numeric questions*/ 
Route::post('add_numeric_question','ApiController@add_numeric_question');

/* add essay questions*/
Route::post('add_essay_question','ApiController@add_essay_question'); 

/* add demographic questions*/ 
Route::post('add_demography_question','ApiController@add_demography_question');

/* questions category list */
Route::post('question_category_list','ApiController@question_category_list');

/* questions subject list*/
Route::post('subjects_list','ApiController@subjects_list'); 

/* mcqs_questions listing*/ 
Route::post('mcqs_list','ApiController@mcqs_list');

/* mrqs_questions listing*/
Route::post('mrqs_list','ApiController@mrqs_list'); 

/* true false questions list*/ 
Route::post('true_false_list','ApiController@true_false_list');

/* short questions list*/
Route::post('short_questions_list','ApiController@short_questions_list');

/* numeric questions list*/
Route::post('numeric_questions_list','ApiController@numeric_questions_list'); 

/* essay typr questions*/
Route::post('essay_questions_list','ApiController@essay_questions_list'); 

/* listing of demographic questions*/
Route::post('demographic_questions_list','ApiController@demographic_questions_list'); 

/* questions list*/
Route::post('questions_list','ApiController@questions_list'); 