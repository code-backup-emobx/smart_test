<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/login', 'LoginController@authenticate')->name('login');
Route::post('/login', 'LoginController@authentication')->name('login');

Route::get('/','HomeController@index');
Route::get('/admin_dashboard','HomeController@admin');
Route::get('/student_dashboard','HomeController@student');
Route::get('/settings', 'HomeController@settings');
Route::get('/setting', 'HomeController@setting');

//SUPER_ADMIN

//superadmin to add the multiple admin
Route::get('add_admin','SuperAdminController@addAdmin');
Route::post('add_admin','SuperAdminController@submitAdmin');

Route::get('/admin_list','SuperAdminController@admin_list');
Route::get('/edit_admin_detail/{id}','SuperAdminController@edit_admin_detail');
Route::post('/edit_admin_detail/{id}','SuperAdminController@update_admin_detail');
Route::get('/delete_admin_detail/{id}','SuperAdminController@delete_admin_detail');


//shows the admin detail to super admin
Route::get('/category_added_by_admin','SuperAdminController@category_added_by_admin');
Route::get('/admin_add_subject_list','SuperAdminController@admin_add_subject_list');

//All Questions Detail added by admin
Route::get('/mcqs_list','SuperAdminController@mcqs_list');
Route::get('/multiple_response_qs','SuperAdminController@multiple_response_qs');
Route::get('/true_false_qs','SuperAdminController@true_false_qs');
Route::get('/short_qs','SuperAdminController@short_qs');
Route::get('/numeric_qs_list','SuperAdminController@numeric_qs_list');
Route::get('/essay_questions_list','SuperAdminController@essay_questions_list');
Route::get('/demographic_qs_list','SuperAdminController@demographic_qs_list');

//End SUPERADMIN ROUTE

//admin functionality route start

/* admin add institute and students*/
Route::get('/add_institutes','InstitutestudentController@add_institute_students');
Route::post('/add_institutes','InstitutestudentController@submit_institute');
Route::get('/institutes_list','InstitutestudentController@institutes_list');
Route::get('/edit_institute/{id}','InstitutestudentController@edit_institute');
Route::post('/edit_institute/{id}','InstitutestudentController@update_institute');
Route::get('/delete_institute/{id}','InstitutestudentController@delete_institute');

/* add students*/ 
Route::get('/add_students','InstitutestudentController@add_students');
Route::post('/add_students','InstitutestudentController@submit_students');
Route::get('/students_list','InstitutestudentController@students_list');


/* end admin add institute and students*/

Route::get('/add_category','CategoryController@add_category');
Route::post('/add_category','CategoryController@submit_category');
Route::get('/category_list','CategoryController@category_list');
Route::get('/edit_category/{id}','CategoryController@edit_category');
Route::post('/edit_category/{id}','CategoryController@update_category');
Route::get('/delete_category_detail/{id}','CategoryController@delete_category_detail');

//add subject
Route::get('/add_subject','CategoryController@add_subject');
Route::post('/add_subject','CategoryController@submit_subject');
Route::get('/subject_list','CategoryController@subject_list');

//edit mcqs question
Route::get('/edit_subject/{id}','CategoryController@edit_subject');
Route::post('/edit_subject/{id}','CategoryController@update_subject');


//add MCQ'S questions
Route::get('/add_questions','QuestionController@add_questions');
Route::post('/add_questions','QuestionController@submit_questions');

// listing of MCQ'S questions
Route::get('/mcqs_qs_list','QuestionController@mcqs_qs_list');
//update MCQS
Route::get('/edit_mcqs_qs/{id}','QuestionController@edit_mcqs_qs');
Route::post('/edit_mcqs_qs/{id}','QuestionController@update_mcqs_qs');
//delete MCQS
Route::get('/delete_question/{id}','QuestionController@delete_question');


//add Multiple Response Question
Route::get('/add_multiple_responce_qs','QuestionController@add_multiple_responce_qs');
Route::post('/add_multiple_responce_qs','QuestionController@submit_multiple_responce_qs');

//multiple response question list 
Route::get('/multiple_response_qs_list','QuestionController@multiple_response_qs_list');

//edit MRQS Questions
Route::get('/edit_mrqs/{id}','QuestionController@edit_mrqs');
Route::post('/edit_mrqs/{id}','QuestionController@update_mrqs');

//delete MRQS Questions
Route::get('/delete_mrqs/{id}','QuestionController@delete_mrqs_question');

//add True False Question
Route::get('/add_true_false','QuestionController@add_true_false');
Route::post('/add_true_false','QuestionController@submit_add_true_false');

//listing
Route::get('/true_false_list','QuestionController@true_false_list');

//edit true false Question
Route::get('/edit_true_false/{id}','QuestionController@edit_true_false');
Route::post('/edit_true_false/{id}','QuestionController@update_true_false');
//Delete true false Question
Route::get('/delete_true_false_statement/{id}','QuestionController@delete_true_false');

//add Short Answer Type Questions
Route::get('/add_short_questions','QuestionController@add_short_questions');
Route::post('/add_short_questions','QuestionController@submit_short_questions');

//list short answer question 
Route::get('/short_question_list','QuestionController@short_question_list');

//edit short answer
Route::get('/edit_short_answer_question/{id}','QuestionController@edit_short_answer_question');
Route::post('/edit_short_answer_question/{id}','QuestionController@update_short_answer');

//delete functionality
Route::get('/delete_short_question/{id}','QuestionController@delete_short_question');

//add Numeric Questions
Route::get('/numeric_questions','QuestionController@add_numeric_questions');
Route::post('/numeric_questions','QuestionController@submit_numeric_questions');

//list of numeric list
Route::get('/numeric_questions_list','QuestionController@numeric_questions_list');

//edit numeric list
Route::get('/edit_numeric_question/{id}','QuestionController@edit_numeric_question');
Route::post('/edit_numeric_question/{id}','QuestionController@update_numeric_question');

//delete numric question
Route::get('/delete_numeric_question/{id}','QuestionController@delete_numeric_question');

//essay type questions
Route::get('/add_essay_question','QuestionController@add_essay_question');
Route::post('/add_essay_question','QuestionController@submit_essay_questions');

//listing of eesay questions
Route::get('/essay_qs_list','QuestionController@essay_qs_list');

//edit essay type questions
Route::get('/edit_essay_qs/{id}','QuestionController@edit_essay_qs');
Route::post('/edit_essay_qs/{id}','QuestionController@update_essay_qs');

//delete question
Route::get('/delete_essay_question/{id}','QuestionController@delete_essay_question');

//add Demographic Question
Route::get('/demographic_questions','QuestionController@add_demographic_questions');
Route::post('/demographic_questions','QuestionController@submit_demographic_questions');

//listing of demograhic questions
Route::get('demographic_questions_list','QuestionController@demographic_questions_list');

//edit demograhic questions
Route::get('/edit_demographic_qs/{id}','QuestionController@edit_demographic_qs');
Route::post('/edit_demographic_qs/{id}','QuestionController@update_demographic_qs');

//delte demographic question
Route::get('/delete_demographic_qs/{id}','QuestionController@delete_demographic_qs');

//admin functionality route end

/* Students route start*/
	/* for public */ 
Route::get('/create_test','StudentController@create_test');
Route::post('/create_test','StudentController@submit_created_test');

Route::get('/choose_test_detail','StudentController@view_addstudent');
Route::post('/choose_test_detail','StudentController@add_username');

Route::get('/choose_type_of_questions','StudentController@choose_type_of_questions');

Route::get('add_questions_for_test/{id}','StudentController@add_question_for_test');
Route::post('add_question_for_test/{id}','StudentController@submit_choose_questions_for_test');
	
	/* for private */ 
Route::post('/add_detail','StudentController@add_detail_for_private_test');
Route::get('/add_test_questions','StudentController@view_add_test');
Route::get('/selected_questions_add/{id}','StudentController@selected_questions_add');
Route::post('/selected_questions_add/{id}','StudentController@submit_selected_questions_add');

/* Students route start*/


Route::get('logout', function()
	{
		Auth::logout();
	    Session::flush();
		return Redirect::to('login');
	});

